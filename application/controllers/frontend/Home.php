<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Frontend_Controller {

	public function __construct() 
	{
        parent::__construct();   
	}
	
	public function index()
	{
		$data = array();
		
		// carica eventi
		$this->db->select('*');
		$this->db->from('eventi');
		$this->db->order_by('evento_id', 'DESC');
		$query_eventi = $this->db->get();
		$data['eventi'] = $query_eventi->result();
		
		// carica immagini
		$this->db->select('*');
		$this->db->from('immagini_gallery');
		$this->db->order_by('ig_ordine', 'ASC');
		$this->db->limit(6);
		$query_gallery = $this->db->get();
		$data['gallery'] = $query_gallery->result();
		
		// carica demo audio
		$data['playlist'] = $this->getDemoPlaylist();
		
		// carica video
		$this->db->select('*');
		$this->db->from('videos');
		$this->db->order_by('v_id', 'DESC');
		$this->db->limit(6);
		$query_video = $this->db->get();
		$data['videos'] = $query_video->result();
		
		$this->load->view('frontend/index', $data);
	}
	
	public function about()
	{
	    $data = array();
	    
	    // carica demo audio
	    $data['playlist'] = $this->getDemoPlaylist();
	    
	    $this->load->view('frontend/about', $data);
	}
	
	public function matrimoni()
	{
		$data = array();
	
		// carica demo audio
		$data['playlist'] = $this->getDemoPlaylist();
		
		$this->load->view('frontend/matrimoni', $data);
	}

	public function contatti()
	{
		$data = array();
		$this->load->view('frontend/contatti', $data);
	}
	
	public function privacy()
	{
		$data = array();
		$this->show_view_with_menu('frontend/privacy', $data);
	}
	
	public function lezioni()
	{
		$data = array();
		
		// carica demo audio
		$data['playlist'] = $this->getDemoPlaylist();
		
		$this->load->view('frontend/lezioni', $data);
	}
	
	public function recensioni()
	{
	    $data = array();
	    
	    // carica reviews
	    $this->db->select('*');
	    $this->db->from('reviews');
	    $this->db->order_by('review_data', 'DESC');
	    $query_review = $this->db->get();
	    $data['reviews'] = $query_review->result();
	    
	    $this->load->view('frontend/recensioni', $data);
	}
	
	public function gallery()
	{	
		$data = array();
        
		// carica immagini
		$this->db->select('*');
		$this->db->from('immagini_gallery');
		$this->db->order_by('ig_ordine', 'ASC');
		$query_gallery = $this->db->get();
		$data['gallery'] = $query_gallery->result();
		
		$this->load->view('frontend/gallery', $data);
	}

	public function getDemoPlaylist() {
	   
	    $this->db->select('*');
	    $this->db->from('demo_audio');
	    $this->db->order_by('da_id', 'DESC');
	    $query_demo = $this->db->get();
	    return $query_demo->result();
	    
	}
}