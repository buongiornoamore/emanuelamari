<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('framework_helper');
	}
	
	public function index()
	{
		$site_url = base_url();
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load languages
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->order_by("id_lingue", "asc");
		$query_lingue = $this->db->get();
		
		foreach ($query_lingue->result() as $lingua)
		{
			array_push($urls, $site_url.strtolower($lingua->abbr_lingue).'/sitemap_pages.xml', 
			                  $site_url.strtolower($lingua->abbr_lingue).'/sitemap_media.xml', 
			                  $site_url.strtolower($lingua->abbr_lingue).'/sitemap_images.xml');
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'general',
					  'urls' => $urls
					  );		
					  
		// load general sitemap info			
		$this->load->view('sitemap', $data);
	}
	
	public function pages($lang_code)
	{
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		// load pages		
		$site_url = base_url();
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load pagine		
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $curr_lang->id_lingue);
		$this->db->where('nome_menu <>', '');
		$query_pages = $this->db->get();

		foreach ($query_pages->result() as $page)
		{
			$tmp_url = $site_url . $page->url_pagina;
			array_push($urls, $this->createUrl($tmp_url, $max_date, $change_freq, $priority));		
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'detail',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function media($lang_code)
	{	
		$urls = array();
		
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->join('pagine', 'lingue.id_lingue = pagine.id_lingua AND pagine.label_page_url = \'PAGE_HOME_URL\'');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();

		$this->db->select('*');
		$this->db->from('videos');
		$this->db->order_by("v_id", "desc");
		
		$query = $this->db->get();
		foreach ($query->result() as $video)
		{
		    $tmp_url = base_url() . $curr_lang->url_pagina;
		    array_push($urls, $this->createVideoUrl($tmp_url, $video->v_link, $video->v_link, $video->v_desc, $video->v_desc));
		}
		
		$data = array(
		    'sitemap_urls_type' => 'video',
		    'urls' => $urls
		);		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function images($lang_code)
	{	
		$urls = array();
		
		// load gallery						
		$this->db->select('*');
		$this->db->from('immagini_gallery');
		$this->db->order_by("ig_id", "desc");
		
		$query = $this->db->get();
		
		foreach ($query->result() as $img)
		{
		    $tmp_url = base_url() . $lang_code . '/gallery';
		    array_push($urls, $this->createImageUrl($tmp_url, ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/bigGallery/'.$img->ig_big_gallery, ' Gallery | ' . $img->ig_nome, SITE_TITLE_NAME.' | '.$img->ig_nome));					
		}
		
		$data = array(
					  'sitemap_urls_type' => 'images',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	/* stampa url per sitemap */
	public function createUrl($url, $lastmod, $change_freq, $priority) {
	
		return '<url>
				<loc>' . $url . '</loc>
				' . ($lastmod != "0000-00-00" ? '<lastmod>' . date_format(date_create($lastmod), "Y-m-d\TH:i:sP") . '</lastmod>' : '') .  ($change_freq != '' ? '<changefreq>' . $change_freq . '</changefreq>' : '') . ($priority != '' ? '<priority>' . $priority . '</priority>' : '') . '
			  </url>';
		  
	}
	
	public function createImageUrl($loc, $loc_image, $caption, $title) {
	
		return '<url>
				<loc>' . $loc . '</loc>
				<image:image>
					<image:loc>' . $loc_image . '</image:loc>
					<image:title>' . $this->string_sanitize($title) . '</image:title>
					<image:caption>' . $this->string_sanitize($caption) . '</image:caption>
				</image:image>	
			  </url>';
		  
	}
	
	public function createVideoUrl($loc_page, $loc_video, $loc_thumb, $desc, $title) {
	    
	    return '<url>
				<loc>' . $loc_page . '</loc>
				<video:video>
					<video:thumbnail_loc>' . $loc_thumb . '</video:thumbnail_loc>
					<video:title>' . $title . '</video:title>
                    <video:description>' . $desc . '</video:description>    
                    <video:content_loc>' . $loc_video . '</video:content_loc>
                 </video:video>
			  </url>';
	    
	}
	
	public function string_sanitize($s) {
		$result = preg_replace("/[^\s\p{L} 0-9|-]/u", "", html_entity_decode($s, ENT_QUOTES));
		return $result;
	}
}