<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminReviews extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('reviews');
			// nome in tabella
			$crud->display_as('Nomi', 'review_nomi');
			$crud->display_as('Data', 'review_data');
			
			// colonne da mostrare
			$crud->change_field_type('review_data','date');
			$crud->columns('review_nomi', 'review_data');
			// unset delete action
			$crud->unset_add();
			
			$crud->unset_texteditor('review_messaggio');
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-REVIEWS';
			$data['curr_page_title'] = 'Recensioni';
			$data['collapseParentMenu'] = 'recensioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/reviews',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
