<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminContatti extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('contatti_moduli');
			// nome in tabella
			$crud->display_as('id_lingua', 'Lingua');
			$crud->display_as('stato_contatto', 'Stato');
			$crud->display_as('data', 'Data contatto');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			$crud->set_relation('stato_contatto', 'stato_descrizione', 'testo_stato_descrizione');
			// colonne da mostrare
			//$crud->change_field_type('data','date');
			$crud->columns('nome', 'email', 'data', 'stato_contatto', 'id_lingua');
			// unset delete action
			$crud->unset_add();
			$crud->unset_texteditor('messaggio');
			$crud->required_fields('nome', 'email', 'stato_contatto', 'messaggio', 'id_lingua');

			$crud->change_field_type('data_unsubscribe', 'date');
			$crud->change_field_type('data', 'date');
		//	$crud->add_action('Invia email', '', '', 'fa-envelope', array($this, 'load_email_templates'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CONTATTI';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/contatti',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
