<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminGallery extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('immagini_gallery');
			$crud->order_by('ig_ordine', 'asc');
			// nome in tabella
			$crud->display_as('ig_content', 'Anteprima');
			$crud->display_as('ig_big_gallery', 'Immagine');
			$crud->display_as('ig_nome', 'Nome immagine');
			$crud->display_as('ig_class', 'Dimensione');
			$crud->display_as('ig_ordine', 'Ordine');
			$crud->display_as('ig_orientamento', 'Orientamento');
			// file upload
			$crud->set_field_upload('ig_content', 'assets/assets-frontend/img/content');
			$crud->set_field_upload('ig_big_gallery', 'assets/assets-frontend/img/bigGallery');
			
			$crud->required_fields('ig_big_gallery', 'ig_nome', 'ig_class', 'ig_orientamento', 'ig_ordine');
		
			// campi per add
			$crud->add_fields('ig_nome', 'ig_class', 'ig_ordine', 'ig_orientamento', 'ig_big_gallery');
			$crud->edit_fields('ig_nome', 'ig_class', 'ig_ordine', 'ig_orientamento', 'ig_big_gallery');
			// callbacks
			$crud->callback_add_field('ig_class', array($this,'_add_field_callback_ig_class'));
			$crud->callback_add_field('ig_orientamento', array($this,'_add_field_callback_ig_orientamento'));
			$crud->callback_edit_field('ig_class', array($this,'_add_field_callback_ig_class'));
			$crud->callback_edit_field('ig_orientamento', array($this,'_add_field_callback_ig_orientamento'));
			$crud->callback_after_insert(array($this, '_callback_insert_gallery_image'));
			$crud->callback_after_update(array($this, '_callback_insert_gallery_image'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_gallery_image'));
			
			// colonne da mostrare
			$crud->columns('ig_content', 'ig_nome', 'ig_ordine');
			// unset delete action
			// $crud->unset_delete();
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-GALLERY';
			$data['curr_page_title'] = 'Gallery';
			$data['collapseParentMenu'] = 'gallery';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/gallery',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _add_field_callback_ig_class($value, $primary_key)
	{
	
	    if(trim($value) != trim('col-sm-12 col-md-6 col-xs-12')) 
	       return '<input type="radio" name="ig_class" value="col-sm-12 col-md-6 col-xs-12" />&nbsp;Grande&nbsp;&nbsp;&nbsp;<input type="radio" name="ig_class" value="col-sm-3 col-xs-6" checked="checked" />&nbsp;Piccola';
	   else 
	       return '<input type="radio" name="ig_class" value="col-sm-12 col-md-6 col-xs-12" checked="checked" />&nbsp;Grande&nbsp;&nbsp;&nbsp;<input type="radio" name="ig_class" value="col-sm-3 col-xs-6" />&nbsp;Piccola';
	
	}
	
	function _add_field_callback_ig_orientamento($value, $primary_key)
	{
	    
	    if(trim($value) != trim('orizzontale'))
	        return '<input type="radio" name="ig_orientamento" value="orizzontale" />&nbsp;Orizzontale&nbsp;&nbsp;&nbsp;<input type="radio" name="ig_orientamento" value="verticale" checked="checked" />&nbsp;Verticale';
        else
            return '<input type="radio" name="ig_orientamento" value="orizzontale" value="verticale" checked="checked" />&nbsp;Orizzontale&nbsp;&nbsp;&nbsp;<input type="radio" name="ig_orientamento" />&nbsp;Verticale';
	            
	}
	
	// Salva il nome dell'immagine thumb se esiste un immagine
	function _callback_insert_gallery_image($post_array, $primary_key) {
		// cancella il file thumb attuale se esiste
		$img = $this->db->where('ig_id', $primary_key)->get('immagini_gallery')->row();
		check_remove_image_file('./assets/assets-frontend/img/content/'.$img->ig_content);
		//check_remove_image_file('./assets/assets-frontend/img/bigGallery/'.$img->ig_big_gallery);
		
		// Sovrascrivi il nome del file thumb e lo ricarica se esiste
		$thumbname = ($post_array['ig_big_gallery'] != '' ? 'cont_'.$post_array['ig_big_gallery'] : '');	
		$data_update = array(
		   'ig_content' => $thumbname
		);
		$this->db->where('ig_id', $primary_key);
		$this->db->update('immagini_gallery', $data_update);
		
		$this->load->library('image_moo');
		// Immagine content
		if(trim($img->ig_class) == trim('col-sm-3 col-xs-6')) {
		    $this->image_moo->load('assets/assets-frontend/img/bigGallery/'.$post_array['ig_big_gallery'])->resize_crop(400, 450)->save('assets/assets-frontend/img/content/cont_'.$post_array['ig_big_gallery'], true);
		} else {
		    $this->image_moo->load('assets/assets-frontend/img/bigGallery/'.$post_array['ig_big_gallery'])->resize_crop(800, 450)->save('assets/assets-frontend/img/content/cont_'.$post_array['ig_big_gallery'], true);
        }
        
        if($img->ig_orientamento == 'verticale')
            $this->image_moo->load('assets/assets-frontend/img/bigGallery/'.$post_array['ig_big_gallery'])->resize_crop(1024, 1536)->save('assets/assets-frontend/img/bigGallery/'.$post_array['ig_big_gallery'], true);
        else
            $this->image_moo->load('assets/assets-frontend/img/bigGallery/'.$post_array['ig_big_gallery'])->resize_crop(1536, 1024)->save('assets/assets-frontend/img/bigGallery/'.$post_array['ig_big_gallery'], true);
	}
	
	// Rimuove immagini collegate
	function _callback_before_delete_gallery_image($primary_key)
	{
		// Carica immagine gallery
		$img = $this->db->where('ig_id', $primary_key)->get('immagini_gallery')->row();
		
		check_remove_image_file('./assets/assets-frontend/img/content/'.$img->ig_content);
		check_remove_image_file('./assets/assets-frontend/img/bigGallery/'.$img->ig_big_gallery);
		return true;
	}
	
}
