<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminUtenti extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('users');
			// nome in tabella
			$crud->display_as('last_name', 'Cognome');
			$crud->display_as('first_name', 'Nome');
			$crud->display_as('email', 'Email');
			$crud->display_as('active', 'Attivo');
			$crud->display_as('password', 'Password');
			
			// colonne da mostrare
			$crud->columns('last_name', 'first_name', 'email', 'active');
			// unset delete action
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_read();
            
		//	$crud->add_fields('ig_nome', 'ig_class', 'ig_ordine', 'ig_orientamento', 'ig_big_gallery');
			$crud->edit_fields('last_name', 'first_name', 'email', 'active', 'password');
			$crud->required_fields('last_name', 'first_name', 'email', 'active', 'password');
			
			//$crud->callback_before_insert(array($this,'_callback_before_insert_user'));
			$crud->callback_before_update(array($this,'_callback_before_update_user'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-USERS';
			$data['curr_page_title'] = 'Utenti';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/utenti',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// check if role was changed and insert new values by new role
	function _callback_before_update_user($post_array) {
	    
	    // encrypt pwd if is new one (clear)
	    if($user->password != $post_array['password'])
	        $post_array['password'] = $this->ion_auth->hash_password($post_array['password'], FALSE, FALSE);
	        
	    return $post_array;
	        
	}
	
}
