<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminVideo extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('videos');
			
			$crud->set_subject('Video');
			
			// nome in tabella
			$crud->display_as('v_link', 'Link video Youtube');
			$crud->display_as('v_desc', 'Descrizione');
	
			$crud->columns('v_link', 'v_desc');
			// unset delete action
			//$crud->unset_add();
			$crud->required_fields('v_link', 'v_desc');

			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-VIDEOS';
			$data['curr_page_title'] = 'Video';
			$data['collapseParentMenu'] = 'video';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/video',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
