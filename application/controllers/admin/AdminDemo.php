<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminDemo extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('demo_audio');
			
			$crud->set_subject('Demo');
			
			$crud->set_field_upload('da_filename','assets/assets-frontend/audio/demo');
			
			// nome in tabella
			$crud->display_as('da_nome', 'Nome');
			$crud->display_as('da_video_link', 'Link video');
			$crud->display_as('da_filename', 'Demo mp3');
	
			$crud->columns('da_nome', 'da_video_link');
			// unset delete action
			//$crud->unset_add();
			$crud->required_fields('da_nome', 'da_filename');
            
			$crud->callback_before_delete(array($this, '_callback_before_delete_demo'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-DEMOS';
			$data['curr_page_title'] = 'Demo';
			$data['collapseParentMenu'] = 'demo';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/demo',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Rimuove demo file
	function _callback_before_delete_demo($primary_key)
	{
	    // Carica immagine gallery
	    $demo = $this->db->where('da_id', $primary_key)->get('demo_audio')->row();  
	    check_remove_image_file('./assets/assets-frontend/audio/demo/'.$demo->da_filename);
	  
	    return true;
	}
	
}
