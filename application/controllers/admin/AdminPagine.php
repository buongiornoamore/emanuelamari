<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminPagine extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('pagine_contenuti');
			$crud->order_by('id_pc', 'asc');
			$crud->order_by('id_lingua', 'asc');
			// file upload
			$crud->set_field_upload('image', 'assets/assets-frontend/img/pages');
			$crud->set_field_upload('image2', 'assets/assets-frontend/img/pages');
			$crud->set_field_upload('background_img', 'assets/assets-frontend/img/pages');
			// nome in tabella
			$crud->display_as('code', 'Pagina');
			$crud->display_as('title', 'Titolo');
			$crud->display_as('id_lingua', 'Lingua');
			$crud->display_as('description', 'Contenuto');
			$crud->display_as('description2', 'Contenuto 2');
			$crud->display_as('meta_description', 'Meta tag');
			$crud->display_as('image', 'Immagine');
			$crud->display_as('image2', 'Immagine 2');
			$crud->display_as('background_img', 'Immagine sfondo');
			$crud->display_as('video', 'Video Youtube');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('code', 'title', 'id_lingua');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_texteditor('description');
			$crud->unset_texteditor('description2');
			$crud->unset_texteditor('meta_description');
			// custom action
			//callbacks
			$crud->callback_after_update(array($this, 'update_lang_callback'));
			// image_moo cropping
		//	$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('code', 'readonly');
			} 
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PAGES';
			$data['curr_page_title'] = 'Configurazioni';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/pages',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Resize dell'immagine caricata e creazione thumb
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
		$this->load->library('image_moo');
		 
		// Sovrascrivo immagine con stesso nome am nuove dimensioni
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
		$this->image_moo->load($file_uploaded)->resize_crop(800, 600)->save($file_uploaded, true);		 
		
		return true;
	}
	
	public function update_lang_callback($post_array, $primary_key)
	{
		// @TODO caricare tutte le pagine di quella lingua
		// pagine_contenuti
		//$this->db->where('id_lingua', $post_array['id_lingua']);
		//$this->db->join('lingue', 'lingue.id_lingue = pagine_contenuti.id_lingua');
	//	$this->db->from('pagine_contenuti');
	//	$query = $this->db->get();
	//	$pagine = $query->result();
		//print_r($this->db->last_query());	
		
		// current row
		$this->db->select('*');
		$this->db->from('pagine_contenuti');
		$this->db->where('id_pc', $primary_key);
		$curr_row = $this->db->get()->row();
		
		// load lingua	
		$lang_id = $curr_row->id_lingua;
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('id_lingue', $lang_id);
		$query_lang = $this->db->get();
		
		// load pagine menu		
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $lang_id);
		$this->db->where('nome_menu !=', "");
		$this->db->order_by("ordine_menu", 'asc');	
		$query_pages = $this->db->get();
		
		// pagine_contenuti
		$this->db->select('*');
		$this->db->from('pagine_contenuti');
		$this->db->where('id_lingua', $lang_id);
		$query_contents = $this->db->get();
		
		// chiama una procedura parametrica passando i dati di interesse e il tipo di labels da generare
		update_lang_file($query_contents->result(), $query_lang->row(), $query_pages->result(), "pages");
		return true;
	}
	
}