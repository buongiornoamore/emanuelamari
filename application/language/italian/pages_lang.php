<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file pages - ITALIANO
*/
/* MENU */
$lang['MENU_HOME'] = "Home";
$lang['MENU_WEDDINGS'] = "Matrimoni";
$lang['MENU_LESSONS'] = "Lezioni";
$lang['MENU_ABOUT'] = "Su di me";
$lang['MENU_REVIEWS'] = "Recensioni";
$lang['MENU_GALLERY'] = "Gallery";
$lang['MENU_PRIVACY'] = "Privacy";
$lang['MENU_CONTACTS'] = "Contatti";
/* HOME */
$lang['PAGE_HOME_CODE'] = "HOME";
$lang['PAGE_HOME_TITLE'] = "Home";
$lang['PAGE_HOME_META_DESCRIPTION'] = "sdfdsf";
$lang['PAGE_HOME_DESCRIPTION'] = "";
$lang['PAGE_HOME_DESCRIPTION2'] = "";
$lang['PAGE_HOME_IMAGE'] = "";
$lang['PAGE_HOME_IMAGE2'] = "";
$lang['PAGE_HOME_VIDEO'] = "";
$lang['PAGE_HOME_BACKGROUND_IMG'] = "";
/* ABOUT */
$lang['PAGE_ABOUT_CODE'] = "ABOUT";
$lang['PAGE_ABOUT_TITLE'] = "Su di me";
$lang['PAGE_ABOUT_META_DESCRIPTION'] = "Questa sono io, se hai bisogno di maggiori informazioni,<br> di un consiglio o  semplicemente se preferisci parlare di persona con me<br> ti invito
a contattarmi e ti risponderò prima possibile.
";
$lang['PAGE_ABOUT_DESCRIPTION'] = "Una bimba che a 10 anni inizia a studiare musica, pianoforte, dopo avere fatto danza classica ( altra mia passione di sempre ) e cantato come voce scelta in tanti cori della scuola… che ormai ragazza si diploma in questo strumento in uno dei Conservatori con la scuola  pianistica più dura e severa: S.Pietro a Majella, a Napoli….
Che intraprende l’insegnamento e continua a suonare qua e là ma…
la voce dell'anima riprende a gridare dentro: Canta….CANTA!
Ok, ricevuto…riprendo a cantare in cori polifonici, girando l’Europa, in competion di vario genere, costruendo una conoscenza sulla musica antica meravigliosa!  ma…..la vocina c’è ancora…NO!
Inizio allora a studiare canto lirico mi avvicino all’opera, all’operetta e….alla canzone napoletana.
";
$lang['PAGE_ABOUT_DESCRIPTION2'] = "Eccomi tornata!!! Nella mia amata Napoli, seconda città. Dove il mio cuore batteva e batte tuttora. Quando da piccola stavo incollata davanti alla televisione a guardare le commedie di Eduardo e ad ascoltare quella musica….il parlare napoletano! Quanto mi ha affascinata!!!
E allora, dopo avere studiato tanta tanta musica ( aggiungete pure jazz, pop, rock, e tutto quello che oggi c’è) avere fatto serate musicali di ogni genere ( compreso il liscio…..) e concerti dove sono approdata???? 
AL TEATRO NAPOLETANO, AL VARIETA’ AL CABARET, ALL’ AVANSPETTACOLO !!!
Oggi Emanuela,  oltre ad essere da sempre insegnante di pianoforte, si dedica, con tanta soddisfazione ( perché al pubblico che ama il genere, PIACE!...al teatro, ai concerti, agli spettacoli.
Adesso mi dovete conoscere…sono proprio unica…..
";
$lang['PAGE_ABOUT_IMAGE'] = "2667f-web1.jpg";
$lang['PAGE_ABOUT_IMAGE2'] = "0c230-web2.jpg";
$lang['PAGE_ABOUT_VIDEO'] = "";
$lang['PAGE_ABOUT_BACKGROUND_IMG'] = "3d20a-about_em.jpg";
/* PRIVACY */
$lang['PAGE_PRIVACY_CODE'] = "PRIVACY";
$lang['PAGE_PRIVACY_TITLE'] = "Privacy";
$lang['PAGE_PRIVACY_META_DESCRIPTION'] = "Scopri in che modo vengo trattati i tuoi dati sensibili,<br>l'utilizzo dei cookie e dei dati di sessione in questo sito.";
$lang['PAGE_PRIVACY_DESCRIPTION'] = "<b>Cookie policy</b><br>
In questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie è una piccola particella di informazioni che viene salvata sul dispositivo dell'utente che visita un sito web. Il cookie non contiene dati personali e non può essere utilizzato per identificare l'utente all'interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l'efficienza del nostro portale.
<br><br>
Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornirà indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br>
In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L'anonimato dell'utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.
<br><br>
Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l’utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell’utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l’accesso in modalità sicura oppure le funzioni di controllo e prevenzione delle frodi.<br><br>
Non è obbligatorio acquisire il consenso alla operatività dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro <br>operatività comporterà l’impossibilità di una corretta navigazione sul Sito e/o la impossibilità di fruire dei servizi, delle pagine, delle funzionalità o dei contenuti ivi disponibili.
<br><br>
Tutti i dati inseriti dai nostri clienti all'interno di moduli e procedure di contatto verranno utilizzati esclusivamente per il completamento degli stessi. 
<br><br>
In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalità od utilizzo.";
$lang['PAGE_PRIVACY_DESCRIPTION2'] = "";
$lang['PAGE_PRIVACY_IMAGE'] = "";
$lang['PAGE_PRIVACY_IMAGE2'] = "";
$lang['PAGE_PRIVACY_VIDEO'] = "";
$lang['PAGE_PRIVACY_BACKGROUND_IMG'] = "37646-matrimonio1.jpg";
/* CONTACTS */
$lang['PAGE_CONTACTS_CODE'] = "CONTACTS";
$lang['PAGE_CONTACTS_TITLE'] = "Contatti";
$lang['PAGE_CONTACTS_META_DESCRIPTION'] = "Se hai bisogno di maggiori informazioni, di un consiglio o <br>semplicemente se preferisci parlare di persona con me ti invito<br>a contattarmi e ti risponderò prima possibile.";
$lang['PAGE_CONTACTS_DESCRIPTION'] = "Se hai bisogno di maggiori informazioni, di un consiglio o <br>semplicemente se preferisci parlare di persona con me ti invito<br>a contattarmi e ti risponderò prima possibile.";
$lang['PAGE_CONTACTS_DESCRIPTION2'] = "";
$lang['PAGE_CONTACTS_IMAGE'] = "";
$lang['PAGE_CONTACTS_IMAGE2'] = "";
$lang['PAGE_CONTACTS_VIDEO'] = "";
$lang['PAGE_CONTACTS_BACKGROUND_IMG'] = "";
/* GALLERY */
$lang['PAGE_GALLERY_CODE'] = "GALLERY";
$lang['PAGE_GALLERY_TITLE'] = "Gallery";
$lang['PAGE_GALLERY_META_DESCRIPTION'] = "Ecco una galleria delle nostre immagini più belle.";
$lang['PAGE_GALLERY_DESCRIPTION'] = "Vi mostro la galleria delle immagini più belle raccolte in questi ultimi anni.";
$lang['PAGE_GALLERY_DESCRIPTION2'] = "";
$lang['PAGE_GALLERY_IMAGE'] = "";
$lang['PAGE_GALLERY_IMAGE2'] = "";
$lang['PAGE_GALLERY_VIDEO'] = "";
$lang['PAGE_GALLERY_BACKGROUND_IMG'] = "";
/* WEDDINGS */
$lang['PAGE_WEDDINGS_CODE'] = "WEDDINGS";
$lang['PAGE_WEDDINGS_TITLE'] = "Matrimoni";
$lang['PAGE_WEDDINGS_META_DESCRIPTION'] = "La musica per il tuo Matrimonio.";
$lang['PAGE_WEDDINGS_DESCRIPTION'] = "La cerimonia di matrimonio è il momento più importante ed emozionante. E allora perché troppe volte trascurarla, o scegliere un accompagnamento che svilisca il tutto? Una voce toccante ed angelica, un accompagnamento musicale che valorizzi ed intensifichi l’emozione nei vari momenti del rito, un prezzo assolutamente concorrenziale, nonostante la mia estrema competenza e preparazione, date da titoli specifici ed una quindicinale esperienza nel settore cerimonie?";
$lang['PAGE_WEDDINGS_DESCRIPTION2'] = "Scegliete con cura chi dovrà esprimere le vostra emozione nel momento più importante del vostro giorno più bello!<br>Per le coppie che sceglieranno la mia musica offro in omaggio il CD contenente tutti i brani della loro cerimonia!<br>Sarà un dolcissimo ricordo che vi accompagnerà sempre, oltre che un prezioso ausilio all’operatore che realizzerà il vostro video di nozze.";
$lang['PAGE_WEDDINGS_IMAGE'] = "matrimonio1.jpg";
$lang['PAGE_WEDDINGS_IMAGE2'] = "matrimonio2.jpg";
$lang['PAGE_WEDDINGS_VIDEO'] = "J2802kgDbc8";
$lang['PAGE_WEDDINGS_BACKGROUND_IMG'] = "";
/* LESSONS */
$lang['PAGE_LESSONS_CODE'] = "LESSONS";
$lang['PAGE_LESSONS_TITLE'] = "Lezioni";
$lang['PAGE_LESSONS_META_DESCRIPTION'] = "Lezioni di musica e canto";
$lang['PAGE_LESSONS_DESCRIPTION'] = "<b>Lezioni individuali di pianoforte (classico, leggero, pianobar) e/o tastiera, anche a domicilio</b><br>
La musica è energia totale. Chi riesce a viaggiare sulla sua lunghezza d'onda lo sa. È un discorso di pancia,  di vibrazione,  di emozione. Il fatto che sia stata codificata in simboli dal significato uguale per il mondo intero,  non deve far dimenticare questo.<br/> 
E quando un bambino dice che gli piace la musica,  che vuole impararla,  vuol dire semplicemente che vuole entrarci tutto,  con tutto se stesso con più consapevolezza. 
Per questo,  oltre al pianoforte,  mi piace farlo esprimere anche attraverso la voce, lasciarlo libero di farla uscire...<br/> 
Perché questo è il modo più naturale che la vita ci ha dato per entrare nel mondo dei suoni. 
E sarà il bambino,  il ragazzo,  che mi farà capire via via dove dobbiamo andare.<br/>
Si,  perché la musica è un mondo senza confini. ";
$lang['PAGE_LESSONS_DESCRIPTION2'] = "";
$lang['PAGE_LESSONS_IMAGE'] = "lezioni1.jpg";
$lang['PAGE_LESSONS_IMAGE2'] = "lezioni2.jpg";
$lang['PAGE_LESSONS_VIDEO'] = "NmQrW_hLDJw";
$lang['PAGE_LESSONS_BACKGROUND_IMG'] = "";
/* REVIEWS */
$lang['PAGE_REVIEWS_CODE'] = "REVIEWS";
$lang['PAGE_REVIEWS_TITLE'] = "Recensioni";
$lang['PAGE_REVIEWS_META_DESCRIPTION'] = "Recensioni su Emanuela Mari";
$lang['PAGE_REVIEWS_DESCRIPTION'] = "Le persone che mi conoscono e con cui ho lavorato hanno detto qualcosa su di me.";
$lang['PAGE_REVIEWS_DESCRIPTION2'] = "";
$lang['PAGE_REVIEWS_IMAGE'] = "";
$lang['PAGE_REVIEWS_IMAGE2'] = "";
$lang['PAGE_REVIEWS_VIDEO'] = "";
$lang['PAGE_REVIEWS_BACKGROUND_IMG'] = "";
