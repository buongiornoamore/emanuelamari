<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file frontend - ITALIANO
*/
/* LANG CONTENTS */
$lang['LANGUAGE_NAME'] = "ITALIANO";
$lang['LANGUAGE_ABBR'] = "IT";
$lang['LANGUAGE_ID'] = "1";
$lang['LANGUAGE_LOCALE_PAYPAL'] = "it_IT";
$lang['LANGUAGE_CI'] = "italian";
/* PAGE URL SHORTCUT */
$lang['PAGE_HOME_URL'] = "it/home";
$lang['PAGE_ABOUT_URL'] = "it/about";
$lang['PAGE_CONTACTS_URL'] = "it/contatti";
$lang['PAGE_PRIVACY_URL'] = "it/privacy";
$lang['PAGE_GALLERY_URL'] = "it/gallery";
$lang['PAGE_WEDDINGS_URL'] = "it/matrimoni";
$lang['PAGE_LESSONS_URL'] = "it/lezioni";
$lang['PAGE_REVIEWS_URL'] = "it/recensioni";
/* LABELS frontend */
$lang['LABEL_INDEX_CLAIM'] = "Musicista | Lezioni | Matrimoni";
$lang['LABEL_INDEX_CLAIM_MOBILE'] = "Musicista Lezioni Matrimoni";
$lang['LABEL_INDEX_VIDEO_LINK'] = "Guarda tutti i video sul canale";
$lang['LABEL_EVENTS'] = "Eventi";
$lang['LABEL_DATE'] = "Data";
$lang['LABEL_LOCATION'] = "Luogo";
$lang['LABEL_NAME'] = "Nome";
$lang['LABEL_TYPE'] = "Tipo";
$lang['LABEL_DETAIL'] = "Dettaglio";
$lang['LABEL_NEXT_EVENT'] = "Prossimo evento";
$lang['LABEL_BACK_TOP'] = "Torna su";
$lang['LABEL_SONG'] = "Brano";
$lang['LABEL_TIME'] = "Durata";
$lang['LABEL_MY_VOICE'] = "Prima di decidere, ascoltate la mia voce!";
$lang['LABEL_FOLLOW_FACEBOOK'] = "Seguimi su Facebook";
$lang['LABEL_CONTACT_INFO'] = "Info di contatto";
$lang['LABEL_CONTACT_INFO_TEXT'] = "Ecco tutti i miei recapiti, se desideri informazioni o chiarimenti non esitare a contattarmi!";
$lang['LABEL_PHONE'] = "Telefono";
$lang['LABEL_SURNAME'] = "Cognome";
$lang['LABEL_MESSAGE'] = "Messaggio";
$lang['LABEL_SEND_MESSAGE'] = "INVIA MESSAGGIO";
$lang['LABEL_CONTACT_ME'] = "Scrivimi";
$lang['LABEL_SEE_MORE'] = "Vedi di più";
$lang['MSG_SEND_CONTACT_US'] = "Ti ringrazio per avermi contattato.<br>Risponderò appena possibile!";