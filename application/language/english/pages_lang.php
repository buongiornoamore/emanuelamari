<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file pages - ENGLISH
*/
/* MENU */
$lang['MENU_HOME'] = "Home";
$lang['MENU_WEDDINGS'] = "Weddings";
$lang['MENU_LESSONS'] = "Lessons";
$lang['MENU_ABOUT'] = "About me";
$lang['MENU_REVIEWS'] = "Reviews";
$lang['MENU_GALLERY'] = "Gallery";
$lang['MENU_PRIVACY'] = "Privacy";
$lang['MENU_CONTACTS'] = "Contacts";
/* HOME */
$lang['PAGE_HOME_CODE'] = "HOME";
$lang['PAGE_HOME_TITLE'] = "Home";
$lang['PAGE_HOME_META_DESCRIPTION'] = "Welcome to Ma Chlò' s website, selling quality garments based on artwork with worldwide shipping.";
$lang['PAGE_HOME_DESCRIPTION'] = "";
$lang['PAGE_HOME_DESCRIPTION2'] = "";
$lang['PAGE_HOME_IMAGE'] = "";
$lang['PAGE_HOME_IMAGE2'] = "";
$lang['PAGE_HOME_VIDEO'] = "";
$lang['PAGE_HOME_BACKGROUND_IMG'] = "";
/* ABOUT */
$lang['PAGE_ABOUT_CODE'] = "ABOUT";
$lang['PAGE_ABOUT_TITLE'] = "About me";
$lang['PAGE_ABOUT_META_DESCRIPTION'] = "This is me, if you need more information,<br/>advice or simply if you prefer to talk in person with me, I invite you
to contact me<br/>and I will answer as soon as possible.";
$lang['PAGE_ABOUT_DESCRIPTION'] = "A girl who at 10 years starts studying music, piano, after doing classical dance (another passion of mine) and sung as a voice chosen in many choirs of the school ... now a girl graduates in this instrument in one of the Conservatories with the harder and more rigorous piano school: S. Pietro a Majella, Naples ....
Who undertakes the teaching and keeps playing here and there but ...
the voice of the soul starts shouting inside: Sing ... SING!
Ok, received ... I resume singing in polyphonic choirs, touring Europe, in various competitions, building a knowledge of wonderful ancient music! but ... .. the voice is still there ... NO!
So I start studying lyric singing and I approach the opera, operetta and ... the Neapolitan song.";
$lang['PAGE_ABOUT_DESCRIPTION2'] = "Here I am back !!! In my beloved Naples, second city. Where my heart was beating and still beating. When I was a child I was glued to the television watching the Eduardo comedies and listening to that music ... .the Neapolitan talk! How fascinated me!And then, after studying so much music (add jazz, pop, rock, and everything that there is today) have made musical evenings of all kinds (including the smooth ... ..) and concerts where I landed ?? ?AT THE NAPOLETANO THEATER, AL VARIETY AT THE CABARET, AT THE AVANSPECTACOLO !!!Today Emanuela, besides being always a piano teacher, dedicates herself with such satisfaction (because to the public who loves the genre, PIACE! ... to theater, concerts, shows.Now you must know me ... I'm really unique ...";
$lang['PAGE_ABOUT_IMAGE'] = "337ec-web1.jpg";
$lang['PAGE_ABOUT_IMAGE2'] = "ae3ee-web2.jpg";
$lang['PAGE_ABOUT_VIDEO'] = "";
$lang['PAGE_ABOUT_BACKGROUND_IMG'] = "14fbc-about_em.jpg";
/* PRIVACY */
$lang['PAGE_PRIVACY_CODE'] = "PRIVACY";
$lang['PAGE_PRIVACY_TITLE'] = "Privacy";
$lang['PAGE_PRIVACY_META_DESCRIPTION'] = "Find out how we treat your sensitive data,<br>use cookies, and sessions data in this site.";
$lang['PAGE_PRIVACY_DESCRIPTION'] = "<b>Cookie policy</b><br>
This site uses some technical cookies that are used for navigation. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the device of the user who visits a website. The cookie does not contain personal data and can not be used to identify you within other websites, including the analytics provider's website. Cookies can also be used to store the preferred settings, such as language and country, in order to make them immediately available on the next visit. We do not use IP addresses or cookies to personally identify users. We use the web analysis system in order to increase the efficiency of our portal.
<br><br>
For more information on cookies we suggest you visit www.allaboutcookies.org which will provide you with information on how to manage according to your preferences, and possibly delete cookies depending on the browser you are using.
On this website, we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browser and users' computers. This information is only examined for statistical purposes. The anonymity of the user is respected. Information on the operation of open source web analytics software Google Analytics.
<br><br>
We reiterate that on the site are operating only technical cookies (such as those listed above) necessary to navigate and essential such as authentication, validation, management of a browsing session and prevention of fraud and allow for example: to identify if the user has had regularly access to areas of the site that require prior authentication or user validation and management of sessions relating to various services and applications or data retention for secure access or fraud control and prevention functions. <br>
It is not mandatory to acquire the consent to the operation of only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial to their operation will make it impossible to correctly browse the site and / or the inability to use the services, pages, features or content available there.
<br><br>
All data entered by our customers within contact forms and procedures will be used exclusively for the completion of the same.
<br><br>
In any case, we confirm that the data used and stored for the purpose of the service will never be transferred to third parties under any circumstances for any purpose or use.";
$lang['PAGE_PRIVACY_DESCRIPTION2'] = "";
$lang['PAGE_PRIVACY_IMAGE'] = "";
$lang['PAGE_PRIVACY_IMAGE2'] = "";
$lang['PAGE_PRIVACY_VIDEO'] = "";
$lang['PAGE_PRIVACY_BACKGROUND_IMG'] = "";
/* CONTACTS */
$lang['PAGE_CONTACTS_CODE'] = "CONTACTS";
$lang['PAGE_CONTACTS_TITLE'] = "Contacts";
$lang['PAGE_CONTACTS_META_DESCRIPTION'] = "If you need more information, advice<br/> or simply if you prefer to talk in person with me,<br/> I invite you to contact me and I will answer as soon as possible.";
$lang['PAGE_CONTACTS_DESCRIPTION'] = "If you need more information, advice<br/> or simply if you prefer to talk in person with me,<br/> I invite you to contact me and I will answer as soon as possible.";
$lang['PAGE_CONTACTS_DESCRIPTION2'] = "";
$lang['PAGE_CONTACTS_IMAGE'] = "";
$lang['PAGE_CONTACTS_IMAGE2'] = "";
$lang['PAGE_CONTACTS_VIDEO'] = "";
$lang['PAGE_CONTACTS_BACKGROUND_IMG'] = "";
/* GALLERY */
$lang['PAGE_GALLERY_CODE'] = "GALLERY";
$lang['PAGE_GALLERY_TITLE'] = "Gallery";
$lang['PAGE_GALLERY_META_DESCRIPTION'] = "This is our best images gallery.";
$lang['PAGE_GALLERY_DESCRIPTION'] = "This is the picture gallery from my portfolio.";
$lang['PAGE_GALLERY_DESCRIPTION2'] = "";
$lang['PAGE_GALLERY_IMAGE'] = "";
$lang['PAGE_GALLERY_IMAGE2'] = "";
$lang['PAGE_GALLERY_VIDEO'] = "";
$lang['PAGE_GALLERY_BACKGROUND_IMG'] = "";
/* WEDDINGS */
$lang['PAGE_WEDDINGS_CODE'] = "WEDDINGS";
$lang['PAGE_WEDDINGS_TITLE'] = "Weddings";
$lang['PAGE_WEDDINGS_META_DESCRIPTION'] = "Music for your wedding";
$lang['PAGE_WEDDINGS_DESCRIPTION'] = "The wedding ceremony is the most important and exciting moment. So why too often neglect it, or choose an accompaniment that debases the whole? A touching and angelic voice, a musical accompaniment that enhances and intensifies the emotion in the various moments of the rite, an absolutely competitive price, despite my extreme competence and preparation, given by specific titles and a fortnightly experience in the ceremonies sector?";
$lang['PAGE_WEDDINGS_DESCRIPTION2'] = "Choose carefully who will express your emotion in the most important moment of your most beautiful day!
For couples who choose my music I offer a free CD containing all the songs of their ceremony!
It will be a sweet memory that will always accompany you, as well as a precious aid to the operator who will make your wedding video.";
$lang['PAGE_WEDDINGS_IMAGE'] = "37646-matrimonio1.jpg";
$lang['PAGE_WEDDINGS_IMAGE2'] = "eb764-matrimonio2.jpg";
$lang['PAGE_WEDDINGS_VIDEO'] = "J2802kgDbc8";
$lang['PAGE_WEDDINGS_BACKGROUND_IMG'] = "";
/* LESSONS */
$lang['PAGE_LESSONS_CODE'] = "LESSONS";
$lang['PAGE_LESSONS_TITLE'] = "Lessons";
$lang['PAGE_LESSONS_META_DESCRIPTION'] = "Music and sing lessons";
$lang['PAGE_LESSONS_DESCRIPTION'] = "<b>Individual piano lessons (classic, light, pianobar) and / or keyboard, even at home </b> <br/>
Music is total energy. Who can travel on its wavelength knows it. It is a speech of the stomach, of vibration, of emotion. The fact that it has been codified in symbols of the same meaning for the whole world, must not make you forget this.<br/>
And when a child says that he likes music, that he wants to learn it, it simply means that he wants to enter everything, with all of himself with more awareness.
For this, in addition to the piano, I like to express it also through the voice, leave it free to let it out ...<br/>
Because this is the most natural way that life has given us to enter the world of sounds.
And it will be the child, the boy, who will make me understand where we have to go.<br/>
Yes, because music is a world without borders.";
$lang['PAGE_LESSONS_DESCRIPTION2'] = "";
$lang['PAGE_LESSONS_IMAGE'] = "lezioni1.jpg";
$lang['PAGE_LESSONS_IMAGE2'] = "lezioni2.jpg";
$lang['PAGE_LESSONS_VIDEO'] = "NmQrW_hLDJw";
$lang['PAGE_LESSONS_BACKGROUND_IMG'] = "";
/* REVIEWS */
$lang['PAGE_REVIEWS_CODE'] = "REVIEWS";
$lang['PAGE_REVIEWS_TITLE'] = "Reviews";
$lang['PAGE_REVIEWS_META_DESCRIPTION'] = "Reviews about Emanuela Mari";
$lang['PAGE_REVIEWS_DESCRIPTION'] = "Read all the reviews from people who knows me and work with me.";
$lang['PAGE_REVIEWS_DESCRIPTION2'] = "";
$lang['PAGE_REVIEWS_IMAGE'] = "";
$lang['PAGE_REVIEWS_IMAGE2'] = "";
$lang['PAGE_REVIEWS_VIDEO'] = "";
$lang['PAGE_REVIEWS_BACKGROUND_IMG'] = "";
