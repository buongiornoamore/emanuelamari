<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file frontend - ENGLISH
*/
/* LANG CONTENTS */
$lang['LANGUAGE_NAME'] = "ENGLISH";
$lang['LANGUAGE_ABBR'] = "EN";
$lang['LANGUAGE_ID'] = "2";
$lang['LANGUAGE_LOCALE_PAYPAL'] = "en_GB";
$lang['LANGUAGE_CI'] = "english";
/* PAGE URL SHORTCUT */
$lang['PAGE_HOME_URL'] = "en/home";
$lang['PAGE_ABOUT_URL'] = "en/about";
$lang['PAGE_CONTACTS_URL'] = "en/contacts";
$lang['PAGE_PRIVACY_URL'] = "en/privacy";
$lang['PAGE_GALLERY_URL'] = "en/gallery";
$lang['PAGE_WEDDINGS_URL'] = "en/weddings";
$lang['PAGE_LESSONS_URL'] = "en/lessons";
$lang['PAGE_REVIEWS_URL'] = "en/reviews";
/* LABELS frontend */
$lang['LABEL_INDEX_CLAIM'] = "Musician | Lessons | Weddings";
$lang['LABEL_INDEX_CLAIM_MOBILE'] = "Musician Lessons Weddings";
$lang['LABEL_INDEX_VIDEO_LINK'] = "Watch all videos on the channel";
$lang['LABEL_EVENTS'] = "Events";
$lang['LABEL_DATE'] = "Date";
$lang['LABEL_LOCATION'] = "Location";
$lang['LABEL_NAME'] = "Name";
$lang['LABEL_TYPE'] = "Type";
$lang['LABEL_DETAIL'] = "Detail";
$lang['LABEL_NEXT_EVENT'] = "Next event";
$lang['LABEL_BACK_TOP'] = "Back on top";
$lang['LABEL_SONG'] = "Song";
$lang['LABEL_TIME'] = "Time";
$lang['LABEL_MY_VOICE'] = "Before deciding, listen to my voice!";
$lang['LABEL_FOLLOW_FACEBOOK'] = "Follow me on facebook";
$lang['LABEL_CONTACT_INFO'] = "Contact infos";
$lang['LABEL_CONTACT_INFO_TEXT'] = "Here are all my contacts, if you want information or clarification do not hesitate to contact me!";
$lang['LABEL_PHONE'] = "Phone";
$lang['LABEL_SURNAME'] = "Surname";
$lang['LABEL_MESSAGE'] = "Message";
$lang['LABEL_SEND_MESSAGE'] = "SEND MESSAGE";
$lang['LABEL_CONTACT_ME'] = "Contact me";
$lang['LABEL_SEE_MORE'] = "See more";
$lang['MSG_SEND_CONTACT_US'] = "Thank you for contacting me.<br/>I will answer as soon as possible!";
