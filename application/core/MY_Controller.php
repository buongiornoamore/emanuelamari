<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('framework_helper');
		$this->load->library('form_validation');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->lang->load('auth', 'italian');
		$this->lang->load('ion_auth');
		$this->load->helper('cookie'); 
		$this->load->library('email');
	}
	
	/* 
		Invia una email con il template di DEFAULT selezionato
		$online_link_show: se mostrare il link di visualizzazione online in alto
		$template_name: nome del tempalte
		$email: indirizzo email a cui inviare (decodificato)
		$send_to_company: true false se inviare la mail in copia alla company
		$show_only: se true mostra la pagina html con la mail altrimenti al invia tramite email senza mostrarla
	*/
	public function send_email_default_template($online_link_show, $template_name, $email, $send_to_company, $show_html, $send_emails) {		
		// WELCOME
		if($template_name == 'welcome') {
			// carica la lingua dalla email del cliente
			$this->db->from('clienti');
			$this->db->join('lingue', 'lingue.id_lingue = clienti.id_lingua');
			$this->db->join('users', 'users.id = clienti.user_id');
			$this->db->where('users.email', $email);
			$query_user = $this->db->get();
			$cliente = $query_user->row();
			// carica traduzioni email
			$labelLangArray = $this->loadLanguageLabels($cliente->id_lingua, '');
			
			// recupera 2 prodotti dallo shop con traduzione
			$this->db->select('*');
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
			$this->db->from('prodotti');
			$this->db->where('prodotti.stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $cliente->id_lingua);
			$this->db->order_by("prodotti.ordine", 'desc');	
			$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
			$this->db->limit(2);
			$query_prods = $this->db->get();
			
			$data = array(
				'template_name' => lang('LABEL_EMAIL_WELCOME_TITLE'),
				'show_unsubscribe_link' => false,
				'unsubscribe_link' => '',
				'show_online_link' => $online_link_show,
				'online_link' => site_url('email/'.$template_name.'/'.urlencode($email)),
				'products' => $query_prods->result(),
				'labelsLang' => $labelLangArray
			);	
			$subject = lang('LABEL_EMAIL_SUBJECT_WELCOME');	
		}
		
		// NEWSLETTER
		if($template_name == 'newsletter') {
			// aggiungi dati degli utlimi prodotti disponibili
			$this->db->from('contatti_newsletter');
			$this->db->join('lingue', 'lingue.id_lingue = contatti_newsletter.lingua_traduzione_id');
			$this->db->where('contatti_newsletter.email_contatto', urldecode($email));
			$query_email = $this->db->get();
			$news_item = $query_email->row();
			
			// carica traduzioni email
			$labelLangArray = $this->loadLanguageLabels($news_item->lingua_traduzione_id, '');
			
			// recupera 2 prodotti dallo shop con traduzione
			$this->db->select('*');
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
			$this->db->from('prodotti');
			$this->db->where('prodotti.stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $news_item->lingua_traduzione_id);
			$this->db->order_by("prodotti.ordine", 'desc');	
			$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
			$this->db->limit(2);
			$query_prods = $this->db->get();
	
			$data = array(
				'template_name' => lang('LABEL_EMAIL_SUBJECT_NEWSLETTER'),
				'show_unsubscribe_link' => true,
				'unsubscribe_link' => site_url('unsubscribe/'.$news_item->codice_ci.'/2/'.urlencode($email)),
				'show_online_link' => $online_link_show,
				'online_link' => site_url('email/'.$template_name.'/'.urlencode($email)),
				'products' => $query_prods->result(),
				'labelsLang' => $labelLangArray
			);	
			$subject = lang('LABEL_EMAIL_SUBJECT_NEWSLETTER');	
		}
		
		// CONTACT
		if($template_name == 'contact') {
			// aggiungi dati degli utlimi prodotti disponibili
			$this->db->from('contatti_moduli');
			$this->db->join('lingue', 'lingue.id_lingue = contatti_moduli.id_lingua');
			$this->db->where('contatti_moduli.email', urldecode($email));
			$query_email = $this->db->get();
			$contatto = $query_email->row();
			
			// carica traduzioni email
			$labelLangArray = $this->loadLanguageLabels($contatto->id_lingua, '');
			
			// recupera 2 prodotti dallo shop con traduzione
			$this->db->select('*');
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
			$this->db->from('prodotti');
			$this->db->where('prodotti.stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $contatto->id_lingua);
			$this->db->order_by("prodotti.ordine", 'desc');	
			$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
			$this->db->limit(2);
			$query_prods = $this->db->get();
	
			$data = array(
				'template_name' => lang('LABEL_EMAIL_SUBJECT_CONTACT'),
				'show_unsubscribe_link' => true,
				'unsubscribe_link' => site_url('unsubscribe/'.$contatto->codice_ci.'/1/'.urlencode($email)),
				'show_online_link' => $online_link_show,
				'online_link' => site_url('email/'.$template_name.'/'.urlencode($email)),
				'products' => $query_prods->result(),
				'labelsLang' => $labelLangArray
			);	
			$subject = lang('LABEL_EMAIL_SUBJECT_CONTACT');	
		}
		
		if($show_html) {
			$this->load->view('admin/html_templates/'.$template_name, $data);
		} 
		if($send_emails) {
			$email_to = $email;
			$body = $this->load->view('admin/html_templates/'.$template_name.'.php', $data, TRUE);
		
			$this->email->clear(TRUE);
			$mail_reply = COMPANY_EMAIL;
			 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($email_to)
				->subject($subject)
				->message($body)
				->send();
				
			// invia anche all'indirizzo COMPANY_EMAIL	
			if($send_to_company) {
				$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($mail_reply)
				->subject($subject. ' - ' . $email_to)
				->message($body)
				->send();
			}
		}
		
	}
	
	/* 
		Invia una email con il template selezionato
		$online_link_show: se mostrare il link di visualizzazione online in alto
		$template_id: id del tempalte sul db
		$email: indirizzo email a cui inviare (decodificato)
		$send_to_company: true false se inviare la mail in copia alla company
		$show_only: se true mostra la pagina html con la mail altrimenti al invia tramite email senza mostrarla
	*/
	public function send_email_custom_template($online_link_show, $template_id, $email, $send_to_company, $show_html, $send_emails) {		
		// decode della email e poi ancora encode se necessario
		$email = urldecode($email);
		
		$this->db->from('email_templates');
		$this->db->join('lingue', 'lingue.id_lingue = email_templates.lingua_traduzione_id');
		$this->db->where('email_templates.id_template', $template_id);
		$query = $this->db->get();
		$temp = $query->row();
		
		// carica traduzioni email
		$labelLangArray = $this->loadLanguageLabels($temp->lingua_traduzione_id, '');
		
		// recupera 2 prodotti dallo shop con traduzione
		$this->db->select('*');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
		$this->db->from('prodotti');
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $temp->lingua_traduzione_id);
		$this->db->order_by("prodotti.ordine", 'desc');	
		$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
		$this->db->limit(2);
		$query_prods = $this->db->get();
			
		$data = array(
			'template_name' => $temp->nome_template,
			'show_unsubscribe_link' => true,
			'unsubscribe_link' => site_url('unsubscribe/'.$temp->codice_ci.'/'.$temp->id_tipo_template.'/'.urlencode($email)),
			'show_online_link' => $online_link_show,
			'online_link' => site_url('email_template/'.$template_id.'/'.urlencode($email)),
			'template_title' => $temp->titolo_template,
			'template_text' => $temp->testo_template,
			'products' => $query_prods->result(),
			'labelsLang' => $labelLangArray
		);	
		
		$subject = $temp->subject_template;
		
		if($show_html) {
			$this->load->view('admin/html_templates/template', $data);
		} 
		if($send_emails) {
			$email_to = $email;
			$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
		
			$this->email->clear(TRUE);
			$mail_reply = COMPANY_EMAIL;
			 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($email_to)
				->subject($subject)
				->message($body)
				->send();
				
			// invia anche all'indirizzo COMPANY_EMAIL	
			if($send_to_company) {
				$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($mail_reply)
				->subject($subject. ' - ' . $email_to)
				->message($body)
				->send();
			}
		}
		
	}	
	
	/* 
		Invia una email con il template selezionato
		$lang_ci_code: codice CI della lingua english/italian
		$type:  tipo labels email/frontend
		Restituisce array con il result della query
	*/
	public function loadLanguageLabels($lang_id, $type) {
		
		$data = array();
		
		$this->db->select('lingue_labels_lang_label, lingue_labels_lang_value');
		$this->db->from('lingue_labels_lang');
		if($type != '')
			$this->db->where('lingue_labels_lang_type', $type);
		$this->db->where('id_lingua', $lang_id);
		
		foreach($this->db->get()->result() as $label) {
			$data[$label->lingue_labels_lang_label] = $label->lingue_labels_lang_value;
		}
		
		$this->db->select('label_page_url, url_pagina');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $lang_id);
		
		foreach($this->db->get()->result() as $page) {
			$data[$page->label_page_url] = $page->url_pagina;
		}
		
		return $data;
		
	}
	
}

class Frontend_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	function show_view_with_menu($view_name, $data) {
		$this->default_controller_mode();
		$this->checkLanguagePermalink();
		
	    // pagine del menù
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('ordine_menu >', 0);
		$this->db->where('id_lingua', lang('LANGUAGE_ID'));
		$this->db->order_by('ordine_menu', 'asc');
		$query_pages = $this->db->get();
		$data['menuPages'] = $query_pages->result();
		
		// lingue
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('stato_lingua >', 0);
		$query_lang = $this->db->get();
		$data['installedLangs'] = $query_lang->result();
		
		// se pagina index carica slider
		if($view_name == 'frontend/index') {
			$this->db->select('*');
			$this->db->from('home_slider');
			$this->db->where('id_lingua', lang('LANGUAGE_ID'));
			$query_slider = $this->db->get();
			$data['homeSlider'] = $query_slider->result();
		}
//  		if(!isset($data)){
//  			log_message('info','>>>>>>>>>> Frontend_Controller > show_view_with_menu() > isset($data)  ' );
//  			redirect($view_name); 
//  		} else {
//  			log_message('info','>>>>>>>>>> Frontend_Controller > show_view_with_menu() > !isset($data)  ' );
// 			// pagine attive o disattive dal menu
// 		    $this->load->view($view_name, $data); // the actual view you wanna load
//  		}

		
		// pagine attive o disattive dal menu
		$this->load->view($view_name, $data); // the actual view you wanna load
	}
	
	function default_controller_mode() {
		// se DEFAULT_CONTROLLER è COMINGSOON redireziona sempre li tranne che per utente admin loggato
		if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
		{
			if($this->router->routes['default_page'] == 'Comingsoon')
				redirect('comingsoon');	
		}
	}
	
	function checkLanguagePermalink() {
	//	echo $this->uri->uri_string();
		if($this->uri->segment(1) != strtolower(lang('LANGUAGE_ABBR'))) {
			$this->db->select('codice_ci');
			$this->db->from('lingue');
			$this->db->where('abbr_lingue', strtoupper($this->uri->segment(1)));
			$query_lang = $this->db->get();
			$curr_lang = $query_lang->row();
			$this->session->set_userdata('site_lang', $curr_lang->codice_ci);
			$this->config->set_item('language', $curr_lang->codice_ci);
			redirect(base_url().$this->uri->uri_string());
		}
	}
	
}

class Admin_Controller extends MY_Controller {
	
	public $site_status = 'offline';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('file_helper');
		$this->load->library('grocery_CRUD');
	}
	
	function checkUserPermissions() 
	{
		if (!$this->ion_auth->logged_in())
		{
			log_message('info','********************* LOGGED IN ** index() - NOT LOGGED ');
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			log_message('info','********************* LOGGED IN ** index() index() - NOT ADMIN ');
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		log_message('info','********************* LOGGED IN ** index() index() - ADMIN ');
	}
	
}