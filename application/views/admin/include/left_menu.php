<div class="se-pre-con" style="display:none"></div> <!-- Page Pre-Loader -->
<div class="sidebar" data-active-color="rose" data-background-color="white" data-image="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/sidebar_default.jpg">
<!--
Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
Tip 2: you can also add an image using data-image tag
Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
    <div class="logo">
        <a href="<?php echo SITE_URL_PATH; ?>" class="simple-text">
            <?// echo COMPANY_NAME; ?>
            <img src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/logo_small.png" />
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="<?php echo SITE_URL_PATH; ?>" class="simple-text">
            <img src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/logo_mini.png" />
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/faces/avatar2.jpg" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <? echo $this->ion_auth->user()->row()->first_name . ' ' . $this->ion_auth->user()->row()->last_name; ?>
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="#">Dati profilo</a>
                        </li>
                        <li>
                            <a href="" onClick="return showConfirmDialog('Conferma logout', 'Sei sicuro di voler abbandonare la sessione?', '', '', '', '', '', '', '<?php echo site_url('admin/logout')?>', 'false'); "><? echo lang("LABEL_LOGOUT"); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
        	<li <? echo ($current_page == 'ADMIN-CONTATTI' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/contatti')?>">
                    <i class="material-icons">contact_mail</i>
                    <p>Contatti</p>
                </a>
            </li>
        	<li <? echo ($current_page == 'ADMIN-REVIEWS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/reviews')?>">
                    <i class="material-icons">star_rate</i>
                    <p>Recensioni</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-EVENTS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/eventi')?>">
                    <i class="material-icons">event</i>
                    <p>Eventi</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-DEMOS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/demos')?>">
                    <i class="material-icons">music_note</i>
                    <p>Demo</p>
                </a>
            </li>
            <li <? echo ($current_page == 'ADMIN-VIDEOS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/videos')?>">
                    <i class="material-icons">videocam</i>
                    <p>Video</p>
                </a>
            </li>
           <!--  <li <//? echo ($current_page == 'ADMIN-SOCIALS' ? 'class="active"' : ''); ?>>
                <a href="<//?php echo site_url('admin/socials')?>">
                    <i class="material-icons">share</i>
                    <p>Social</p>
                </a>
            </li> -->
            <li <? echo ($current_page == 'ADMIN-GALLERY' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/gallery')?>">
                    <i class="material-icons">photo_camera</i>
                    <p>Gallery</p>
                </a>
            </li>
            <? if($this->ion_auth->is_admin()) { ?>
             <li>
                <a data-toggle="collapse" href="#configurazioni" class="collapsible-link" data-id="configurazioni">
                    <i class="material-icons">settings</i>
                    <p>Configurazioni
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="configurazioni">
                    <ul class="nav">
                    	<li <? echo ($current_page == 'ADMIN-IMPOSTAZIONI' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/impostazioni')?>">
                                <i class="material-icons">build</i>
                                <p>Impostazioni</p>
                            </a>
                        </li>  
                        <li <? echo ($current_page == 'ADMIN-LANGUAGES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/languages')?>">
                                <i class="material-icons">language</i>
                                <p>Lingue</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-PAGES' ? 'class="active"' : ''); ?>>
							<a href="<?php echo site_url('admin/pages')?>">
								<i class="material-icons">content_paste</i>
								<p>Pagine</p>
							</a>
						</li>
						<li <? echo ($current_page == 'ADMIN-USERS' ? 'class="active"' : ''); ?>>
							<a href="<?php echo site_url('admin/utenti')?>">
								<i class="material-icons">perm_identity</i>
								<p>Utenti</p>
							</a>
						</li>
						<? } ?>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
		var parentMenuActive = '<?php echo $data['collapseParentMenu']; ?>';
		if(parentMenuActive != '' && !$('#'+parentMenuActive).attr("aria-expanded")){
			$('#'+parentMenuActive).collapse(); 	
		}
		
		$('.collapsible-link').on('click', function(e){
		   e.preventDefault();
			console.log($(this).data('id'));
		   if(parentMenuActive != '' && $(this).data('id') != parentMenuActive) {
			 return true;
		   } else {
		   	return false;
		   }
		});	
	});	
</script>
