<!DOCTYPE html>
<html lang="en">
<head>
	<title><? echo lang('PAGE_HOME_TITLE'); ?> - Emanuela Mari</title>	
    <meta name="description" content="<? echo lang('PAGE_HOME_META_DESCRIPTION'); ?>" />
  	<? include('include/common_header_css.php'); ?> 
</head>
<body>
	
    <div class="page-loader">
        <div class="vertical-align align-center">
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/loader/loader.gif" alt="" class="loader-img">
        </div>
    </div>
	<!-- PLAYER e LOADER -->
    <? include('include/player.php'); ?> 

	<!-- PLAYLIST -->
    <? include('include/playlist.php'); ?> 
	
    <!-- Header Top Menu -->
	<? include('include/top_header.php'); ?> 

	<!-- =============== START BREADCRUMB CENTRALE ================ -->
	<section class="no-mb">
		<div class="row">
			<div class="col-sm-12">
				<div class="breadcrumb-fullscreen-parent phone-menu-bg">
					<div class="breadcrumb breadcrumb-fullscreen alignleft small-description overlay almost-black-overlay" style="background-image: url('<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/starHomePage/star.jpg');" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
						<div id="home" style="position: absolute;left: 0;top: 0;">
							<div class="intro-header">
								<div class="js-height-full star" style="height: 955px;">
									<div class="star-pattern-1 js-height-full" style="height: 994px;"></div>
									<div class="col-sm-12"> 
										<div class="starTitle">
											<h4><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/logo/logo_white.png" alt="Emanuela Mari" style="width:200px;"></h4>
											<div class="grid__item">
						                		<h1>
								                	<a class="link link-yaku" href="home">
														<span>E</span>
                                                        <span>M</span>
                                                        <span>A</span>
                                                        <span>N</span>
                                                        <span>U</span>
                                                        <span>E</span>
                                                        <span>L</span>
                                                        <span>A</span>
                                                        &nbsp;
                                                        <span>M</span>
                                                        <span>A</span>
                                                        <span>R</span>
                                                        <span>I</span>					
													</a>
										        </h1>
						                	</div>
											<h4 class="mobile-hide"><? echo lang('LABEL_INDEX_CLAIM'); ?></h4>
											<h4 class="mobile-show"><? echo lang('LABEL_INDEX_CLAIM_MOBILE'); ?></h4>
										</div>
										<canvas class="cover" width="1920" height="955"></canvas>
									</div>
								</div>
							</div>
	   					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- =============== END BREADCRUMB ================ -->
   <!-- <section class="videoHome">
    	Accompagnamento Musicale Matrimoni
        Lezioni di Piano a Roma
		Commento musica e voce per eventi e cerimonie
	</section>-->
	<!-- =============== START VIDEO SECTION ================ -->
	<section class="videoHome padding">
		<div class="container">
			<div class="row">
				<div class="sectionTitle">
					<span class="heading-t3"></span>
					<h2>Video</h2>
					<span class="heading-b3"></span>
					<p><? echo lang('LABEL_INDEX_VIDEO_LINK'); ?> <a href="https://www.youtube.com/channel/UCpduN-1PZz45xZL--KA7kfg" target="_blank">Youtube - Emanuela Mari</a>.</p>
				</div><!-- end sectionTtile -->
				<?php foreach ($videos as $v) { ?>
				<div class="col-sm-4">
					<iframe width="560" height="315" src="<?php echo $v->v_link; ?>" allowfullscreen></iframe>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<!-- =============== END VIDEO SECTION ================ -->
	<!-- =============== START GALLERY SECTION ================ -->
	<section style="padding-bottom:0; padding-top:0;">
		<div class="gallerySection">
			<div class="container-fluid" style="padding:0;">
				<div class="col-sm-12">
					<div class="content-container clearfix">
						<div class="single-photo-album-container">
							<div class="row">
							<?php foreach ($gallery as $img) { ?>
                                <article class="<?php echo $img->ig_class;?>">
									<figure>
										<figcaption>
											<div class="hovereffect">
												<img class="img-responsive" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/content/<?php echo $img->ig_content;?>" alt="<?php echo $img->ig_nome;?>">
												<div class="overlay">
												   <a class="info lightbox" href="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/bigGallery/<?php echo $img->ig_big_gallery;?>"></a>
												</div>
											</div>
										</figcaption>
									</figure>
								</article>
    							<?php } ?>				
							</div>
						</div>
					</div>
				</div>
			</div><!-- end container -->
		</div>
	</section>
	<!-- =============== END GALLERY SECTION ================ -->
	<!-- =============== START ICONS MENU SECTION ================ -->
	<section class="padding">
		<div class="container container-short-menu">
			<div class="row">
				<div class="col-sm-2 col-sm-offset-1 link-short-menu">
					<a href="<? echo createUrlMenu(lang('PAGE_ABOUT_URL')); ?>" class="icon-link-short-menu"><i class="fa fa-music icon-link-short-menu-icon"></i><br/><br/><?php echo lang('MENU_ABOUT'); ?></a>
				</div>
				<div class="col-sm-2 link-short-menu">
					<a href="<? echo createUrlMenu(lang('PAGE_LESSONS_URL')); ?>" class="icon-link-short-menu"><i class="fa fa-graduation-cap icon-link-short-menu-icon"></i><br/><br/><?php echo lang('MENU_LESSONS'); ?></a>
				</div>
				<div class="col-sm-2 link-short-menu">
					<a href="<? echo createUrlMenu(lang('PAGE_WEDDINGS_URL')); ?>" class="icon-link-short-menu"><i class="fa fa-gift icon-link-short-menu-icon"></i><br/><br/><?php echo lang('MENU_WEDDINGS'); ?></a>
				</div>
				<div class="col-sm-2 link-short-menu">
					<a href="<? echo createUrlMenu(lang('PAGE_REVIEWS_URL')); ?>" class="icon-link-short-menu"><i class="fa fa-star icon-link-short-menu-icon"></i><br/><br/><?php echo lang('MENU_REVIEWS'); ?></a>
				</div>
				<div class="col-sm-2 link-short-menu">
					<a href="<? echo createUrlMenu(lang('PAGE_CONTACTS_URL')); ?>" class="icon-link-short-menu"><i class="fa fa-envelope icon-link-short-menu-icon"></i><br/><br/><?php echo lang('MENU_CONTACTS'); ?></a>
				</div>
			</div>
		</div>
	</section>			
	<!-- =============== END ICONS MENU SECTION ================ -->
	<!-- =============== START EVENTS SECTION-1 ================ -->
	<section style="background-image: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/events/home-events-1.jpg);" class="background-properties paddingHomeEvents">
		<div class="tableEvents">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="sectionTitle paddingBottom">
							<span class="heading-t3"></span>
							<h2><? echo lang('LABEL_EVENTS'); ?></h2>
							<span class="heading-b3"></span>
						</div><!-- end sectionTtile -->
						<!-- <table>
							<tr class="tableEventsTitle">
								<th class="date"><//? echo lang('LABEL_DATE'); ?></th>
								<th class="venue"><//? echo lang('LABEL_LOCATION'); ?></th>
								<th class="location"><//? echo lang('LABEL_NAME'); ?></th>
								<th class="tickets"><//? echo lang('LABEL_TYPE'); ?></th>
								<th></th>
							</tr>
							<//?php foreach ($eventi as $evento) { ?>
                            <tr>
								<td class="event-text aqura-date"><//?php echo $evento->evento_periodo;?></td>
								<td class="event-text aqura-location"><//?php echo $evento->evento_location;?></td>
								<td class="event-text aqura-city"><//?php echo $evento->evento_nome;?></td>
								<td class="event-text aqura-tickets"><//?php echo $evento->evento_tipo;?></td>
								<td class="event-text aqura-vip"><a href="<//?php echo $evento->evento_link;?>" target="_blank"><//? echo lang('LABEL_DETAIL'); ?></a></td>
							</tr>
							<//?php } ?>
						</table>-->
						<!-- mobile eventes view -->
						<?php foreach ($eventi as $evento) { ?>
						<div class="col-sm-12 events-div" align="center">
							<div class="col-sm-12 events-div-child events-div-title-bold"><?php echo $evento->evento_nome; ?></div>
							<div class="col-sm-12 events-div-child events-div-title"><?php echo $evento->evento_location; ?></div>
							<div class="col-sm-12 events-div-child"><?php echo $evento->evento_periodo; ?></div>
							<div class="col-sm-12 events-div-child "><a href="<?php echo $evento->evento_link;?>" target="_blank" class="events-div-button"><? echo lang('LABEL_DETAIL'); ?></a></div>
						</div>
						<?php } ?>
					</div><!-- end col-sm-12 -->
				</div><!-- end row -->
			</div><!-- end container -->
		</div><!-- end tableEvents -->
	</section>
	<!-- =============== END EVENTS SECTION-1 ================ -->
	
	<!-- =============== START EVENTS SECTION-2 ================ -
	<section class="padding countdownSection background-properties" style="background-image: url(<//? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/events/nextEvent.jpg);">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="countdownTitle">
						<h2><//? echo lang('LABEL_NEXT_EVENT'); ?></h2>
                        <br/>
                        <br/>
                       	<h3>16/10/2017</h3>
                        <br/>
                        <h3 style="font-size:36px;">Jetta a mare 'e guaje</h3>
                        <br/>
                        <h4><b>Roma - Teatro delle Muse</b></h4>
						<a href="singleEvent.html" class="btn btn-primary btn-round">
                            <//? echo lang('LABEL_DETAIL'); ?>
                        </a>
					</div>
					<div class="sm-countdown sm_content_element sm-style2" id="sm_countdown-19" data-date="2017/10/16">
						<div class="displayCounter">
							<div class="column">
								<div class="sm_countdown_inner">
									<input class="element days" readonly data-min="0" data-max="365" data-width="200" data-height="200" data-thickness="0.15" data-fgcolor="#fff" data-bgcolor="#8e8d8d" data-angleoffset="180">
									<span class="unit days-title">days</span>
								</div>
							</div>	
							<div class="column">
								<div class="sm_countdown_inner">
									<input class="element hour" readonly data-min="0" data-max="24" data-width="200" data-height="200" data-thickness="0.15" data-fgcolor="#fff" data-bgcolor="#8e8d8d" data-angleoffset="180">
									<span class="unit hours-title">hrs</span>
								</div>
							</div>	
							<div class="column"> 
								<div class="sm_countdown_inner">
									<input class="element minute" readonly data-min="0" data-max="60" data-width="200" data-height="200" data-thickness="0.15" data-fgcolor="#fff" data-bgcolor="#8e8d8d" data-angleoffset="180">
									<span class="unit mins-title">min</span>
								</div>
							</div>
							<div class="column"> 
								<div class="sm_countdown_inner">
									<input class="element second" readonly data-min="0" data-max="60" data-width="200" data-height="200" data-thickness="0.15" data-fgcolor="#fff" data-bgcolor="#8e8d8d" data-angleoffset="180">
									<span class="unit secs-title">sec</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>->
	<!-- =============== END EVENTS SECTION-2 ================ -->

	<!-- Footer -->
    <? include('include/footer.php'); ?>

	<!-- JS FILES -->
    <? include('include/common_header_js.php'); ?>
</body>
</html>