<!DOCTYPE html>
<html lang="en">
<head>
	<title><? echo lang('PAGE_GALLERY_TITLE'); ?> - Emanuela Mari</title>	
    <meta name="description" content="<? echo lang('PAGE_GALLERY_META_DESCRIPTION'); ?>" />
	<? include('include/common_header_css.php'); ?>
</head>
<body >
	<div class="page-loader">
         <div class="vertical-align align-center">
              <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/loader/loader.gif" alt="" class="loader-img">
         </div>
    </div>
	<!-- PLAYER e LOADER
    <?// include('include/player.php'); ?> 

	<!-- PLAYLIST 
    <?// include('include/playlist.php'); ?> -->
	
    <!-- Header Top Menu -->
	<? $top_header_color = 'black';
	   include('include/top_header.php'); ?> 
	
	<!-- =============== START BIOGRAPHY SECTION ================ -->
	<section style="" class="biography padding background-properties" id="content">
		<div class="container">
			<div class="sectionTitle paddingBottom">
				<span class="heading-t3"></span>
				<h2><? echo lang('MENU_GALLERY'); ?></h2>
				<span class="heading-b3"></span>
			</div><!-- end sectionTtile -->
			<div class="row">
                <div class="col-sm-12">
                	<br/>
                	<p align="center" style="font-size: 26px;color:#bb9b69"><? echo lang('PAGE_GALLERY_DESCRIPTION'); ?></p>
                </div>
			</div>
		</div><!-- end container -->
	</section>
	<!-- =============== END BIOGRAPHY SECTION ================ -->
	
    <!-- =============== START GALLERY SECTION ================ -->
	<section style="padding-bottom:0; padding-top:0;">
		<div class="gallerySection">
			<div class="container-fluid" style="padding:0;">
				<div class="col-sm-12">
					<!-- Content Container -->
					<div class="content-container clearfix">
						<div class="single-photo-album-container">
							<div class="row">
								<?php foreach ($gallery as $img) { ?>
                                <article class="<?php echo $img->ig_class;?>">
									<figure>
										<figcaption>
											<div class="hovereffect">
												<img class="img-responsive" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/content/<?php echo $img->ig_content;?>" alt="<?php echo $img->ig_nome;?>">
												<div class="overlay">
												   <a class="info lightbox" href="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/bigGallery/<?php echo $img->ig_big_gallery;?>"></a>
												</div>
											</div>
										</figcaption>
									</figure>
								</article>
    							<?php } ?>								
							</div>
						</div>
					</div>
				</div>
			</div><!-- end container -->
		</div>
	</section>
	<!-- =============== END GALLERY SECTION ================ -->
	
    <!-- Contatti -->
	<? include('include/contact_module.php'); ?> 
                    
	<!-- Footer -->
    <? include('include/footer.php'); ?>

	<!-- JS FILES -->
    <? include('include/common_header_js.php'); ?>
</body>
</html>