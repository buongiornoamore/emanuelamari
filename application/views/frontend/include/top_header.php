<!-- =============== START TOP HEADER ================ -->
<div class="topHeader" <?php echo (isset($top_header_color) && $top_header_color != '' ? 'style="background-color:'.$top_header_color.';"' : '');?>>
    <div class="header">
        <div class="rightTopHeader">
        	<!-- Language switch -->
        	<? if(lang('LANGUAGE_ABBR') == 'EN') {?> 
        	<a id="italian-flag" href="<?php echo base_url(); ?>LangSwitch/switchLanguage/italian/<? echo $this->uri->segment(1).'/'.$this->uri->segment(2).($this->uri->segment(3) != '' ? '/'.$this->uri->segment(3) : '').($this->uri->segment(4) != '' ? '/'.$this->uri->segment(4) : ''); ?>" title="Versione in Italiano"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flags/italian.png" alt="italian" width="35px"></a>
            <? } else { ?>
            <a id="english-flag" href="<?php echo base_url(); ?>LangSwitch/switchLanguage/english/<? echo $this->uri->segment(1).'/'.$this->uri->segment(2).($this->uri->segment(3) != '' ? '/'.$this->uri->segment(3) : '').($this->uri->segment(4) != '' ? '/'.$this->uri->segment(4) : ''); ?>" title="English language version"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flags/english.png" alt="english" width="35px"></a>
            <? } ?>
            <!-- Open Menu Button -->
            <a class="open-menu">
                <!-- Buttons Bars -->
                <span class="span-1"></span>
                <span class="span-2"></span>
                <span class="span-3"></span>
            </a>
        </div><!-- end rightTopHeader -->
    </div><!-- end header -->
    <!-- Menu Fixed Container -->
    <div class="menu-fixed-container">
        <!-- Menu Fixed Centred Container -->
        <nav>
            <!-- Menu Fixed Close Button -->
            <div class="x-filter">
                <span></span>
                <span></span>
            </div>
            <!-- Menu Fixed Primary List -->
            <ul>
                <!-- Menu Fixed Item -->
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_HOME_URL')); ?>">
                        <? echo lang('MENU_HOME'); ?>
                    </a>
                </li>
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_ABOUT_URL')); ?>">
                        <? echo lang('MENU_ABOUT'); ?>
                    </a>
                </li>
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_LESSONS_URL')); ?>">
                        <? echo lang('MENU_LESSONS'); ?>
                    </a>
                </li>
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_WEDDINGS_URL')); ?>">
                        <? echo lang('MENU_WEDDINGS'); ?>
                    </a>
                </li>
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_GALLERY_URL')); ?>">
                        <? echo lang('MENU_GALLERY'); ?>
                    </a>
                </li>
            	<li>
                    <a href="<? echo createUrlMenu(lang('PAGE_REVIEWS_URL')); ?>">
                        <? echo lang('MENU_REVIEWS'); ?>
                    </a>
                </li>
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_CONTACTS_URL')); ?>">
                        <? echo lang('MENU_CONTACTS'); ?>
                    </a>
                </li>
                <li>
                    <a href="<? echo createUrlMenu(lang('PAGE_PRIVACY_URL')); ?>">
                        <? echo lang('MENU_PRIVACY'); ?>
                    </a>
                </li>
            </ul>
            <!-- Menu Fixed Close Button -->
            <div class="x-filter">
                <span></span>
                <span></span>
            </div>
        </nav>
    </div><!-- end menu-fixed-container -->
    <!-- =============== STAR LOGO ================ -->
    <div class="logo-container-top">
        <a href="home" class="logo_white">
            <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/logo/logo_white.png" alt="Emanuela Mari" style="width:30px;">
            EMANUELA MARI
        </a>
    </div><!-- end logo-container-top -->
    <!-- =============== END LOGO ================ -->
</div><!-- end topHeader -->
<!-- =============== END TOP HEADER ================ -->