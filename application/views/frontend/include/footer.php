<!-- =============== START FOOTER ================ -->
<section style="background-color:#eeeeee;">
    <div class="footer footerPadding">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="copyFooter">
                        <a href="home">
                        	&copy; Emanuela Mari 
							<script>
                				document.write(new Date().getFullYear())
            				</script>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6" align="center">
                    <nav class="social-icons">
                        <ul class="clearfix">
                            <li>
                                <a href="<? echo FACEBOOK_LINK; ?>" class="icon-button shopIcon" title="Emanuela Mari | Facebook">
                                    <i class="fa fa-facebook"></i><span></span>
                                </a>
                            </li>
                            <li>
                                <a href="<? echo INSTAGRAM_LINK; ?>" class="icon-button shopIcon" title="Emanuela Mari | Instagram">
                                    <i class="fa fa-instagram"></i><span></span>
                                </a>
                            </li>								
                            <li>
                                <a href="<? echo YOUTUBE_LINK; ?>" class="icon-button shopIcon" title="Emanuela Mari | YouTube">
                                    <i class="fa fa-youtube-play"></i><span></span>
                                </a>
                            </li>
                            <li>
                                <a href="callto:<? echo COMPANY_PHONE; ?>" class="icon-button shopIcon" title="Emanuela Mari | <? echo lang('LABEL_PHONE'); ?>">
                                    <i class="fa fa-phone"></i><span></span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:<? echo COMPANY_EMAIL; ?>" class="icon-button shopIcon" title="Emanuela Mari | Email">
                                    <i class="fa fa-envelope"></i><span></span>
                                </a>
                            </li>  
                            <li>
                                <a href="<? echo FACEBOOK_2_LINK; ?>" class="icon-button shopIcon" title="La cerimonia perfetta | Facebook">
                                    <i class="fa fa-facebook"></i><span></span>
                                </a>
                            </li>
                            <li>
                                <a href="<? echo FACEBOOK_3_LINK; ?>" class="icon-button shopIcon" title="Napoletanamente insieme | Facebook">
                                    <i class="fa fa-facebook"></i><span></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-3">
                    <div class="goTop back-to-top" id="back-to-top">
                        <i class="fa fa-angle-up"></i>
                        <a href="#"><? echo lang('LABEL_BACK_TOP'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =============== END FOOTER ================ -->