<!-- ================================================== -->
<!-- =============== START JQUERY SCRIPTS ================ -->
<!-- ================================================== -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/jquery.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/bootstrap.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/jplayer/jplayer/jquery.jplayer.js" type="text/javascript"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/jPlayer.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/plugins.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/main.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/commonScripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<!--[if lte IE 9 ]>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/placeholder.js"></script>
<script>
    jQuery(function() {
        jQuery('input, textarea').placeholder();
    });
</script>
<![endif]-->
<!-- ================================================== -->
<!-- =============== END JQUERY SCRIPTS ================ -->
<!-- ================================================== -->
<script type="text/javascript">
	$(window).load(function() {
		// contact module
		$('#submit-btn').on('click', function(e) {
			e.preventDefault();
			$('#submit-btn').attr('disabled', 'disabled');
			if(
				validateText('nome') &
				validateEmail('email') &
				validateText('cognome') &
				validateText('messaggio')
			)
			{
			  var formData = $('#contacts-form').serialize();
			  $.ajax({
					url: '<? echo base_url();?>mail/contactus',
					type: 'POST',
					dataType: "HTML",
					async: true,
					data: formData,
					error: function(msg){
						$('#submit-btn').removeAttr('disabled');
						swal({
						  position: 'center',
						  type: 'error',
						  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
						  showConfirmButton: false,
						  timer: 3000
						});
						return msg;
					},
					success: function(html){
						$('#submit-btn').removeAttr('disabled');
					//	$('#contacts-message').html(html);
						//$("#contacts-message-div").show();
						//$('#contacts-form').hide();
						$('#contacts-form')[0].reset();
						// messagge
						swal({
						  position: 'center',
						  type: 'success',
						  title: "<?php echo lang('MSG_SEND_CONTACT_US'); ?>",
						  showConfirmButton: false,
						  timer: 2000
						});
						return true;
					}
				});
			} else {
				$('#submit-btn').removeAttr('disabled');
				console.log('failed');
			}
		}); 
		$("#nome").on("keyup blur", function(){validateText('nome');});
		$("#cognome").on("keyup blur", function(){validateText('cognome');});
		$("#messaggio").on("keyup blur", function(){validateText('messaggio');});
		$("#email").on("keyup blur", function(){validateEmail('email');});
    }); 
</script>