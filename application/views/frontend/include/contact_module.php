<!-- =============== START CONTACT ================ -->
<section class="contactSingle padding background-properties" id="content" style="background-image:url(<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/contact/contact1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="contactTop">
                    <h2><? echo lang('LABEL_CONTACT_INFO'); ?></h2>
                    <h4>Emanuela mari - Roma</h4>
                    <p>
                    	<? echo lang('LABEL_CONTACT_INFO_TEXT'); ?>
                    </p>
                    <div class="contactInfo">
                        <ul>
                            <li>Email: <a href="#"><? echo COMPANY_EMAIL; ?></a></li>
                            <li><? echo lang('LABEL_PHONE'); ?>: <a href="#"> <? echo COMPANY_PHONE; ?></a></li>
                            <li>Whatsapp: <a href="#"> <? echo COMPANY_PHONE; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- end col-sm-4 -->
            <div class="col-sm-7 col-sm-offset-1">
                <div class="singleBlogForm">
                    <h2><? echo lang('LABEL_CONTACT_ME'); ?></h2>
                    <form action="#" method="post" class="contacts-form" id="contacts-form">
                        <input id="nome" name="nome" type="text" value="" aria-required="true" required="" placeholder="<? echo lang('LABEL_NAME'); ?> *">
                        <input id="cognome" name="cognome" type="text" value="" aria-required="true" required="" placeholder="<? echo lang('LABEL_SURNAME'); ?> *">
                        <input id="telefono" name="telefono" type="text" value="" placeholder="<? echo lang('LABEL_PHONE'); ?> ">
                        <input id="email" name="email" type="text" value="" placeholder="Email * ">
                        <textarea id="messaggio" name="messaggio" placeholder="<? echo lang('LABEL_MESSAGE'); ?> *" rows="6" required></textarea>
                        <p class="form-submit">
                            <input name="submit" type="button" id="submit-btn" value="<? echo lang('LABEL_SEND_MESSAGE'); ?>">
                        </p>
                    </form>
                </div><!-- end contactForm -->
            </div><!-- end col-sm-7 col-sm-offset-1 -->
        </div>
    </div><!-- end container -->
</section>
<!-- =============== END CONTACT ================ -->