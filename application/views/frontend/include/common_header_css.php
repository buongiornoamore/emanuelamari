<meta charset="UTF-8">
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="manifest" href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<!--Favicon-->
<link rel="shortcut icon" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.ico" type="image/x-icon">
<meta name="theme-color" content="#ffffff">
<!-- ========== CSS INCLUDES ========== -->
<link rel="stylesheet" href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/custom.css">
<link rel="stylesheet" href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/master.css">

<!-- GoogleAnalytics -->
<?//php include_once("analyticstracking.php") ?>
<!-- Smartsupp -->
<?//php include_once("smartsupp.php") ?>
<!-- Cookie law plugin -->
<?//php include_once("cookielaw.php") ?>