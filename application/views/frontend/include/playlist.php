<!-- =============== START PLAYLIST ================ -->
<div class="playlist-wrapper" id="playlist-wrapper">
    <div class="jp-playlist container">
        <div class="about-list clearfix">
            <span class="about-name"><? echo strtoupper(lang('LABEL_SONG')); ?></span>
            <span class="about-length"><? echo strtoupper(lang('LABEL_TIME')); ?></span>
            <span class="about-available"></span>
        </div>    
        <?php 
            foreach ($playlist as $demo) { ?>
        	<div class="trak-item" data-audio="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/audio/demo/<?php echo $demo->da_filename; ?>" data-artist="Emanuela Mari" data-thumbnail="assets/img/albums/cover_ema.jpg" data-id="trak-200">
            <audio preload="metadata" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/audio/demo/<?php echo $demo->da_filename; ?>" title="<?php echo $demo->da_nome; ?>"></audio>
            <div class="additional-button">
                <div class="center-y-table">
                 	<?php if($demo->da_video_link != '') {?>
                 	<a href="<?php echo $demo->da_video_link; ?>" title="YouTube" target="_blank">
                        <i class="fa fa-youtube-play"></i>
                    </a>
                    <?php } ?>
                    <a href="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/audio/demo/<?php echo $demo->da_filename; ?>" title="Download Mp3" download>
                        <i class="fa fa-download"></i>
                    </a>
                </div>
            </div>
            <div class="play-pause-button">
                <div class="center-y-table">
                    <i class="fa fa-play" title="Play"></i>
                </div>
            </div>
            <div class="name-artist">
                <div class="center-y-table">
                    <h2>
                        <?php echo $demo->da_nome; ?>
                    </h2>
                </div>
            </div>
            <time class="trak-duration">
                00:00
            </time>
        </div>
        <?php }?>
    </div>
</div>
<!-- =============== END PLAYLIST ================ -->