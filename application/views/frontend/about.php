<!DOCTYPE html>
<html lang="en">
<head>
	<title><? echo lang('PAGE_ABOUT_TITLE'); ?> - Emanuela Mari</title>	
    <meta name="description" content="<? echo lang('PAGE_ABOUT_META_DESCRIPTION'); ?>" />
	<? include('include/common_header_css.php'); ?>
</head>
<body >
	<div class="page-loader">
         <div class="vertical-align align-center">
              <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/loader/loader.gif" alt="" class="loader-img">
         </div>
    </div>
	<!-- PLAYER e LOADER
    <? include('include/player.php'); ?> 

	<!-- PLAYLIST 
    <? include('include/playlist.php'); ?> 
	
    <!-- Header Top Menu -->
	<? include('include/top_header.php'); ?> 

	<!-- Header Top Menu -->
	<? include('include/top_header.php'); ?> 

	<!-- =============== START BREADCRUMB ================ -->
	<section class="no-mb">
		<div class="row">
			<div class="col-sm-12">
				<div class="before-FullscreenSlider"></div>
				<div class="breadcrumb-fullscreen-parent phone-menu-bg">
					<div class="breadcrumb breadcrumb-fullscreen alignleft small-description overlay almost-black-overlay" style="background-image: url('<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/pages/<? echo lang('PAGE_ABOUT_BACKGROUND_IMG');?>');" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
						<div class="breadTxt">
                            <h1>
                                <? echo lang('MENU_ABOUT'); ?>
                            </h1>
                            <p>
                                <? echo lang('PAGE_ABOUT_META_DESCRIPTION'); ?>
                            </p>
                            <a href="#content" data-easing="easeInOutQuint" data-scroll="" data-speed="900" data-url="false">
                                <? echo lang('LABEL_SEE_MORE'); ?> <i class="fa fa-angle-down"></i>
                            </a>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- =============== END BREADCRUMB ================ -->
    
    <!-- =============== START BIOGRAPHY SECTION ================ -->
	<section style="background-image:url(assets/img/content/bio2.jpg);" class="biography padding background-properties" id="content">
		<div class="container">
			<div class="sectionTitle paddingBottom">
				<span class="heading-t3"></span>
				<h2><? echo lang('MENU_ABOUT'); ?></h2>
				<span class="heading-b3"></span>
			</div><!-- end sectionTtile -->
			<div class="row">
				<div class="col-sm-6">
                	<img class="img-responsive" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/pages/<? echo lang('PAGE_ABOUT_IMAGE'); ?>" alt="<? echo lang('PAGE_ABOUT_META_DESCRIPTION'); ?>">
				</div>
				<div class="col-sm-6">
                	<img class="img-responsive" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/pages/<? echo lang('PAGE_ABOUT_IMAGE2'); ?>" alt="<? echo lang('PAGE_ABOUT_META_DESCRIPTION'); ?>">
				</div>
                <div class="col-sm-12">
                	<br/>
                	<p align="justify"><? echo lang('PAGE_ABOUT_DESCRIPTION'); ?></p>
                    <br/>
                    <p align="justify"><? echo lang('PAGE_ABOUT_DESCRIPTION2'); ?></p>	
                </div>
                <div class="col-sm-12" align="center" style="margin-top:25px;font-size:18px;background-color:#bb9b69;color:#fff;padding:10px">
                	<a href="demo" id="demoBtn" style="color:#fff !important;"><b><? echo lang('LABEL_MY_VOICE'); ?></b></a>
                </div>
			</div>
		</div><!-- end container -->
	</section>
	<!-- =============== END BIOGRAPHY SECTION ================ -->
	
    <!-- Contatti -->
	<? include('include/contact_module.php'); ?> 
                    
	<!-- Footer -->
    <? include('include/footer.php'); ?>

	<!-- JS FILES -->
    <? include('include/common_header_js.php'); ?>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('.hide-player-button').click();
		$("#demoBtn").on("click", function(e) {
			e.preventDefault();
			$('.hide-player-button').click();
			$('.jplayerButton').click();
		});	
	});
</script>	
</html>