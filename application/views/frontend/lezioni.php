<!DOCTYPE html>
<html lang="en">
<head>
	<title><? echo lang('PAGE_LESSONS_TITLE'); ?> - Emanuela Mari</title>
    <meta name="description" content="<? echo lang('PAGE_LESSONS_META_DESCRIPTION'); ?>" />
	<? include('include/common_header_css.php'); ?>
</head>
<body >
	<div class="page-loader">
         <div class="vertical-align align-center">
              <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/loader/loader.gif" alt="" class="loader-img">
         </div>
    </div>
	<!-- PLAYER e LOADER -->
    <? include('include/player.php'); ?> 

	<!-- PLAYLIST -->
    <? include('include/playlist.php'); ?> 
	
    <!-- Header Top Menu -->
	<? include('include/top_header.php'); ?> 

	<!-- =============== START BREADCRUMB ================ -->
	<section class="no-mb mobile-hide">
        <div class="row">
            <div class="col-sm-12">
            	<div class="breadcrumb-fullscreen-parent phone-menu-bg">
					<div class="breadcrumb breadcrumb-fullscreen alignleft small-description overlay almost-black-overlay" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
						<div class="starTitle starVideo">
							<h4></h4>
							<div class="grid__item">
		                		<h1>
				                	<a class="link link-yaku" href="#">
										<? 
											echo addSpanToChars(lang('MENU_LESSONS'));
										?>
									</a>
						        </h1>
		                	</div>
							<h4>Emanuela Mari</h4>
						</div>
						<div id="bgndVideo" class="player" data-property="{videoURL:'<? echo lang('PAGE_LESSONS_VIDEO'); ?>',containment:'.player',autoPlay:true, mute:false, startAt:0, opacity:1}">
                    	    
                    	</div>
					</div>
				</div><!--end bread  -->
            </div>
        </div>
    </section>
	<!-- =============== END BREADCRUMB ================ -->
	
	<!-- =============== START BIOGRAPHY SECTION ================ -->
	<section style="background-image:url(assets/img/content/bio2.jpg);" class="biography padding background-properties" id="content">
		<div class="container">
			<div class="sectionTitle paddingBottom">
				<span class="heading-t3"></span>
				<h2><? echo lang('MENU_LESSONS'); ?></h2>
				<span class="heading-b3"></span>
			</div><!-- end sectionTtile -->
			<div class="row">
				<div class="col-sm-6">
                	<img class="img-responsive" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/pages/<? echo lang('PAGE_LESSONS_IMAGE'); ?>" alt="<? echo lang('PAGE_LESSONS_META_DESCRIPTION'); ?>">
				</div>
				<div class="col-sm-6">
                	<img class="img-responsive" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/img/pages/<? echo lang('PAGE_LESSONS_IMAGE2'); ?>" alt="<? echo lang('PAGE_LESSONS_META_DESCRIPTION'); ?>">
				</div>
                <div class="col-sm-12">
                	<br/>
                	<p align="justify"><? echo lang('PAGE_LESSONS_DESCRIPTION'); ?></p>
                    <br/>
                    <p align="justify"><? echo lang('PAGE_LESSONS_DESCRIPTION2'); ?></p>	
                </div>
                <div class="col-sm-12" align="center" style="margin-top:25px;font-size:18px;background-color:#bb9b69;color:#fff;padding:10px">
                	<a href="demo" id="demoBtn" style="color:#fff !important;"><b><? echo lang('LABEL_MY_VOICE'); ?></b></a>
                </div>
			</div>
		</div><!-- end container -->
	</section>
	<!-- =============== END BIOGRAPHY SECTION ================ -->
	
    <!-- JS FILES -->
    <? include('include/common_header_js.php'); ?>
        
    <!-- Contatti -->
	<? include('include/contact_module.php'); ?> 
                    
	<!-- Footer -->
    <? include('include/footer.php'); ?>

</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('.hide-player-button').click();
		$("#demoBtn").on("click", function(e) {
			e.preventDefault();
			$('.hide-player-button').click();
			$('.jplayerButton').click();
		});	
	});
</script>	
</html>