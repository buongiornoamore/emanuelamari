<!DOCTYPE html>
<html lang="en">
<head>
	<title><? echo lang('PAGE_REVIEW_TITLE'); ?> - Emanuela Mari</title>	
    <meta name="description" content="<? echo lang('PAGE_REVIEW_META_DESCRIPTION'); ?>" />
	<? include('include/common_header_css.php'); ?>
</head>
<body >
	<div class="page-loader">
         <div class="vertical-align align-center">
              <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND; ?>/loader/loader.gif" alt="" class="loader-img">
         </div>
    </div>
	<!-- PLAYER e LOADER
    <?// include('include/player.php'); ?> 

	<!-- PLAYLIST 
    <?// include('include/playlist.php'); ?> -->
	
    <!-- Header Top Menu -->
	<? $top_header_color = 'black';
	   include('include/top_header.php'); ?> 
	
    <!-- =============== START ALBUM SINGLE ================ -->
	<section class="albumSingle padding background-properties" style="background-image:url(assets/img/albums/single11.jpg);">
		<div class="container">
			<div class="sectionTitle paddingBottom">
				<span class="heading-t3"></span>
				<h2><? echo lang('MENU_REVIEWS'); ?></h2>
				<span class="heading-b3"></span><br/>
				<span style="color:#bb9b69"><? echo lang('PAGE_REVIEWS_DESCRIPTION'); ?></span>
			</div><!-- end sectionTtile -->
			<div class="row">		
				<div class="col-sm-12">
					<div class="jp-playlist">
						<?php foreach ($reviews as $rev) { ?>
						<div class="trak-item" data-audio="assets/audio/flute.mp3" data-artist="Tensnake" data-thumbnail="assets/img/albums/cover1.jpg">
							<div style="padding-top:10px;padding-bottom:10px;font-size: 1.4rem;">
								<div class="center-y-table">
									<h2>
										<b><?php echo $rev->review_nomi; ?></b> - <?php echo formattaData($rev->review_data, 'd/m/Y'); ?>
									</h2>
									<p align="justify" style="margin-top:5px;font-size: 1.5rem;font-family: 'Karla', sans-serif;"><?php echo $rev->review_messaggio; ?></p>
								</div>
							</div>
						</div>
						<?php } ?>	
					</div>
				</div><!-- end-col-sm-8 -->
			</div><!-- end row -->		
		</div><!-- end container -->
	</section>
	<!-- =============== END ALBUM SINGLE ================ -->
	
    <!-- Contatti -->
	<? include('include/contact_module.php'); ?> 
                    
	<!-- Footer -->
    <? include('include/footer.php'); ?>

	<!-- JS FILES -->
    <? include('include/common_header_js.php'); ?>
</body>
</html>