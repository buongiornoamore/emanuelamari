<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Global frmework */
/* SITE_URL_ROOT è definita nella ROOT index.php */
define('SITE_URL_PATH',		               		'http://localhost/emanuelamari');
define('SITE_TITLE_NAME',		               	'Emanuela Mari dev');
/* Email smtp global vars */
define('SMTP_HOST_CUSTOM',                      'secureit20.sgcpanel.com');
define('SMTP_USER_CUSTOM',                      'info@emanuelamari.it');
define('SMTP_PASS_CUSTOM',                      'ema17@Info');
/* Database MySql global vars */
define('DB_HOSTNAME',	    	                'localhost');
define('DB_USERNAME',	    	                'root');
define('DB_PASSWORD',	    	                '');
define('DB_DATABASE',	    	                'emanu405_framework');
/* Company global vars */
define('COMPANY_NAME',		                    'Emanuela Mari');
define('COMPANY_EMAIL',		                    'info@emanuelamari.it');
define('COMPANY_COPYRIGHT',		                '&copy; ' . date("Y") . ' ' . COMPANY_NAME);
define('COMPANY_PHONE',		                    '+39 3388968472');
define('COMPANY_ADDRESS',		                '');
define('GPLUS_LINK',		                    '');
define('YOUTUBE_LINK',		                    'https://www.youtube.com/channel/UCpduN-1PZz45xZL--KA7kfg');
define('PINTEREST_LINK',		                '');
define('TWITTER_LINK',		                    '');
define('FACEBOOK_LINK',                         'https://www.facebook.com/emanuela.mari.9');
define('FACEBOOK_2_LINK',                       'https://www.facebook.com/Lacerimoniaperfetta/');
define('FACEBOOK_3_LINK',                       'https://it-it.facebook.com/napoletanamenteinsieme/');
define('INSTAGRAM_LINK',                        'https://www.instagram.com/lamanu_mari/');
define('GOOGLE_ANALITYCS_ID',		            '');
/* Coming soon OPTIONS */
define('COMNINGSOON_LOGO',		                'logo.png');
define('COMNINGSOON_BACK_IMAGE',		        'default.jpg');
define('COMNINGSOON_BTN_COLOR',		            '83634D');
// Genearl constants
require_once(APPPATH . 'config/constants_general.php');
/* End of file constants.php */
/* Location: ./application/config/constants.php */
?>