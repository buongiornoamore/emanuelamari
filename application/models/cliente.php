<?
class Cliente extends CI_Model {

	public $id_cliente;
	public $cognome = '';
	public $nome = '';
	public $data_nascita = '';
	public $partita_iva = '';
	public $codice_fiscale = '';
	public $email = '';
	public $telefono_fisso = '';
	public $cellulare = '';
	public $user_id;
	public $fax = '';


	public function get_Cliente_by_User_id($user_id)
	{

		log_message('info', '>> get_Cliente_by_User_id : 1 ' );

		$this->db->select('clienti.*, indirizzo_fatturazione.indirizzo_fatt AS indirizzo_fatt, indirizzo_fatturazione.civico_fatt AS civico_fatt, indirizzo_fatturazione.cap_fatt AS cap_fatt, indirizzo_fatturazione.nazione_fatt AS nazione_fatt, indirizzo_fatturazione.citta_fatt AS citta_fatt, indirizzo_fatturazione.riferimento_fatt AS riferimento_fatt, indirizzo_fatturazione.note_fatt AS note_fatt');
		$this->db->from('clienti');
		$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_indirizzo_fatturazione = clienti.id_indirizzo_fatturazione', 'left');
		$this->db->where('clienti.user_id', $user_id);

		log_message('info', '>> get_Cliente_by_User_id : 2 ' . $user_id );

		$query = $this->db->get();
		$cliente_info = $query->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );

		return $cliente_info;
		
	}

	public function get_Ordini_by_Cliente($user_id)
	{

		log_message('info', '>> get_Ordini_by_Cliente : 1 ' );

		$this->db->select('id_cliente');
		$this->db->from('clienti');
		$this->db->where('user_id', $user_id);

		log_message('info', '>> get_Ordini_by_Cliente : 2 ' . $user_id );

		$query_cliente = $this->db->get();
		$cliente = $query_cliente->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );

		
		$this->db->select('*');
		$this->db->from('ordini');
		$this->db->join('stato_ordine', 'stato_ordine.id_stato_ordine = ordini.stato_ordine');
		$this->db->join('stato_pagamento', 'stato_pagamento.id_stato_pagamento = ordini.stato_pagamento');
		$this->db->where('id_cliente', $cliente->id_cliente);
		$this->db->order_by('ordini.id_ordine', 'DESC');
		
		$query_ordine = $this->db->get();
		
		
		return $query_ordine->result();


	}

	public function get_storico_carrello_by_Ordine($id_ordine)
	{
	
		log_message('info', '>> get_Ordini_by_Cliente : 1 ' );
	 //prodotti.*,
		$this->db->select('storico_carrello.*');
		$this->db->from('storico_carrello');
	//	$this->db->join('prodotti', 'prodotti.id_prodotti = storico_carrello.id_prodotto');
	//	$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = storico_carrello.id_variante');
	//	$this->db->join('ordini', 'ordini.id_ordine = storico_carrello.id_ordine');
	//	$this->db->join('indirizzo_spedizione', 'indirizzo_spedizione.id_indirizzo_spedizione = ordini.id_indirizzo_spedizione', 'left');
	//	$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_indirizzo_fatturazione = ordini.id_indirizzo_fatturazione_spedizione', 'left');
		$this->db->where('storico_carrello.id_ordine', $id_ordine);
	
		$query_ordine = $this->db->get();
	
	
		return $query_ordine->result();
	
	
	}
	
	public function get_Indirizzi_Spedizione_by_Cliente($user_id)
	{
	
		log_message('info', '>> get_Ordini_by_Cliente : 1 ' );
	
		$this->db->select('id_cliente');
		$this->db->from('clienti');
		$this->db->where('user_id', $user_id);
	
		log_message('info', '>> get_Ordini_by_Cliente : 2 ' . $user_id );
	
		$query_cliente = $this->db->get();
		$cliente = $query_cliente->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );
	
	
		$this->db->select('*');
		$this->db->join('countries', 'countries.country_code = indirizzo_spedizione.nazione_sped');
		$this->db->from('indirizzo_spedizione');
		$this->db->where('indirizzo_spedizione.id_cliente', $cliente->id_cliente);
		$this->db->order_by("indirizzo_spedizione.flag_predefinito_sped", 'desc');	
	
		$query_indirizzi_spedizione = $this->db->get();
	
		$indirizzi_sped = $query_indirizzi_spedizione->result();
		
		return $indirizzi_sped;
	}
	
	public function get_wishlist_by_Cliente($user_id)
	{
	
		log_message('info', '>> get_wishlist_by_Cliente : 1 ' );
	
		$this->db->select('id_cliente');
		$this->db->from('clienti');
		$this->db->where('user_id', $user_id);
	
		log_message('info', '>> get_wishlist_by_Cliente : 2 ' . $user_id );
	
		$query_cliente = $this->db->get();
		$cliente = $query_cliente->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );
	
	
		$this->db->select('wishlist.*, varianti_prodotti.*, prodotti.id_prodotti AS id_prodotti, prodotti_traduzioni.*');
		$this->db->join('varianti_prodotti', 'varianti_prodotti.codice = wishlist.codice_variante', "LEFT");
		$this->db->join('prodotti', 'prodotti.codice = varianti_prodotti.codice_prodotto', "LEFT");
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->from('wishlist');
		$this->db->where('wishlist.id_cliente', $cliente->id_cliente);
		//$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->order_by("wishlist.data_creazione", 'desc');	
		$query_wish = $this->db->get();
		//print_r($this->db->last_query());
		
		return $query_wish->result();

	}
}
?>
