-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ago 23, 2018 alle 16:22
-- Versione del server: 10.1.33-MariaDB
-- Versione PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emanu405_framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.emanuelamari.it', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'Emanuela Mari', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'secureit20.sgcpanel.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'info@emanuelamari.it', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'ema17@Info', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '77.104.188.200', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'emanu405', 'Utente del DB'),
(8, 'DB_PASSWORD', 'e18@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'emanu405_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'Emanuela Mari', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'info@emanuelamari.it', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', '© 2018 Emanuela Mari', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39 3388968472', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', '', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', 'https://www.youtube.com/channel/UCpduN-1PZz45xZL--KA7kfg', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/emanuela.mari.9', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/lamanu_mari/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', '', 'UID Google Analitycs'),
(23, 'COMNINGSOON_LOGO', 'logo.png', 'File logo del COMING SOON'),
(24, 'COMNINGSOON_BACK_IMAGE', 'default.jpg', 'File immagine di sfondo Coming soon'),
(25, 'COMNINGSOON_BTN_COLOR', '83634D', 'Colore del pulsante Coming soon'),
(35, 'FACEBOOK_2_LINK', 'https://www.facebook.com/Lacerimoniaperfetta/', 'Link social FACEBOOK'),
(36, 'FACEBOOK_3_LINK', 'https://it-it.facebook.com/napoletanamenteinsieme/', 'Link social FACEBOOK');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `messaggio` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `messaggio`, `data`, `stato_contatto`, `id_lingua`) VALUES
(1, 'Ciro', 'cirozzo32423423@dsfdsfsd.ouit', '4234234', 'Ciaoooo', '2018-08-22 22:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `demo_audio`
--

CREATE TABLE `demo_audio` (
  `da_id` int(11) NOT NULL,
  `da_nome` varchar(255) NOT NULL,
  `da_video_link` varchar(255) NOT NULL,
  `da_filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `demo_audio`
--

INSERT INTO `demo_audio` (`da_id`, `da_nome`, `da_video_link`, `da_filename`) VALUES
(1, 'Ave Maria - Gounod', '', 'ave_maria_gounod.mp3'),
(2, 'C\'era una volta il West', '', 'cera_una_volta_il_west.mp3'),
(3, 'Fratello Sole sorella Luna', 'https://www.youtube.com/watch?v=IsH9HXsdv_A', 'fratello_sole_sorella_luna.mp3'),
(4, 'La vita è bella', '', 'la_vita_e_bella.mp3'),
(5, 'Memory', '', 'memory.mp3'),
(6, 'Over the rainbow', '', 'over_the_rainbow.mp3'),
(7, 'Panis Angelicus', '', 'panis_angelicus.mp3'),
(8, 'The Lord\'s prayer', '', 'the_lords_prayer.mp3'),
(9, 'Vierge Marie Siougos', '', 'virge_marie_siougos.mp3');

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `subject_template` varchar(250) NOT NULL,
  `titolo_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `id_tipo_template` int(11) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `eventi`
--

CREATE TABLE `eventi` (
  `evento_id` int(11) NOT NULL,
  `evento_periodo` varchar(250) NOT NULL,
  `evento_nome` varchar(250) NOT NULL,
  `evento_location` varchar(250) NOT NULL,
  `evento_tipo` varchar(150) NOT NULL,
  `evento_link` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `eventi`
--

INSERT INTO `eventi` (`evento_id`, `evento_periodo`, `evento_nome`, `evento_location`, `evento_tipo`, `evento_link`) VALUES
(1, '10/05/2018 al 13/05/2018', 'Turisti sciantose e... \'nu babbà', 'Teatro Petrolini', 'Spettacolo', 'https://m.facebook.com/story.php?story_fbid=2042890979301187&id=1902995216624098');

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery`
--

CREATE TABLE `immagini_gallery` (
  `ig_id` int(11) NOT NULL,
  `ig_nome` varchar(250) NOT NULL,
  `ig_big_gallery` varchar(250) NOT NULL,
  `ig_content` varchar(250) NOT NULL,
  `ig_class` varchar(50) NOT NULL,
  `ig_ordine` int(11) NOT NULL DEFAULT '0',
  `ig_orientamento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery`
--

INSERT INTO `immagini_gallery` (`ig_id`, `ig_nome`, `ig_big_gallery`, `ig_content`, `ig_class`, `ig_ordine`, `ig_orientamento`) VALUES
(1, 'Prima immagine', '1.jpg', 'g1.jpg', 'col-sm-12 col-md-6 col-xs-12\r\n', 1, 'orizzontale'),
(2, 'Seconda immagine', '2.jpg', 'g2.jpg', 'col-sm-3 col-xs-6', 2, 'orizzontale'),
(3, 'Terza immagine', '3.jpg', 'g3.jpg', 'col-sm-3 col-xs-6', 3, 'verticale'),
(4, 'Quarta immagine', '4.jpg', 'g4.jpg', 'col-sm-3 col-xs-6', 4, 'verticale'),
(5, 'Quinta immagine', '5.jpg', 'g5.jpg', 'col-sm-3 col-xs-6', 5, 'orizzontale'),
(6, 'Sesta immagine', '6.jpg', 'g6.jpg', 'col-sm-12 col-md-6 col-xs-12', 6, 'orizzontale');

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels_lang`
--

CREATE TABLE `lingue_labels_lang` (
  `id_lingue_labels_lang` int(11) NOT NULL,
  `lingue_labels_lang_label` varchar(250) NOT NULL,
  `lingue_labels_lang_value` text NOT NULL,
  `lingue_labels_lang_desc` varchar(250) NOT NULL,
  `lingue_labels_lang_type` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels_lang`
--

INSERT INTO `lingue_labels_lang` (`id_lingue_labels_lang`, `lingue_labels_lang_label`, `lingue_labels_lang_value`, `lingue_labels_lang_desc`, `lingue_labels_lang_type`, `id_lingua`) VALUES
(1, 'LABEL_INDEX_CLAIM', 'Musicista | Lezioni | Matrimoni', 'Etichette in home page sotto il nome', 'frontend', 1),
(2, 'LABEL_INDEX_CLAIM', 'Musician | Lessons | Weddings', 'Etichette in home page sotto il nome', 'frontend', 2),
(3, 'LABEL_INDEX_VIDEO_LINK', 'Guarda tutti i video sul canale', 'Link per guarda tutti i video', 'frontend', 1),
(4, 'LABEL_INDEX_VIDEO_LINK', 'Watch all videos on the channel', 'Link per guarda tutti i video', 'frontend', 2),
(5, 'LABEL_EVENTS', 'Eventi', 'Eventi', 'frontend', 1),
(6, 'LABEL_EVENTS', 'Events', 'Eventi', 'frontend', 2),
(7, 'LABEL_DATE', 'Data', 'Data ', 'frontend', 1),
(8, 'LABEL_DATE', 'Date', 'Data ', 'frontend', 2),
(9, 'LABEL_LOCATION', 'Luogo', 'Luogo', 'frontend', 1),
(10, 'LABEL_LOCATION', 'Location', 'Luogo', 'frontend', 2),
(11, 'LABEL_NAME', 'Nome', 'Nome', 'frontend', 1),
(12, 'LABEL_NAME', 'Name', 'Nome', 'frontend', 2),
(13, 'LABEL_TYPE', 'Tipo', 'Tipo', 'frontend', 1),
(14, 'LABEL_TYPE', 'Type', 'Tipo', 'frontend', 2),
(15, 'LABEL_DETAIL', 'Dettaglio', 'Dettaglio', 'frontend', 1),
(16, 'LABEL_DETAIL', 'Detail', 'Dettaglio', 'frontend', 2),
(17, 'LABEL_NEXT_EVENT', 'Prossimo evento', 'Prossimo evento', 'frontend', 1),
(18, 'LABEL_NEXT_EVENT', 'Next event', 'Prossimo evento', 'frontend', 2),
(19, 'LABEL_BACK_TOP', 'Torna su', 'Torna su', 'frontend', 1),
(20, 'LABEL_BACK_TOP', 'Back on top', 'Torna su', 'frontend', 2),
(21, 'LABEL_SONG', 'Brano', 'Brano', 'frontend', 1),
(22, 'LABEL_SONG', 'Song', 'Brano', 'frontend', 2),
(23, 'LABEL_TIME', 'Durata', 'Durata', 'frontend', 1),
(24, 'LABEL_TIME', 'Time', 'Durata', 'frontend', 2),
(25, 'LABEL_MY_VOICE', 'Prima di decidere, ascoltate la mia voce!', 'Prima di decidere ascolta la mia voce', 'frontend', 1),
(26, 'LABEL_MY_VOICE', 'Before deciding, listen to my voice!', 'Prima di decidere ascolta la mia voce', 'frontend', 2),
(27, 'LABEL_FOLLOW_FACEBOOK', 'Seguimi su Facebook', 'Seguimi su facebook La cerimonia perfetta', 'frontend', 1),
(28, 'LABEL_FOLLOW_FACEBOOK', 'Follow me on facebook', 'Seguimi su facebook La cerimonia perfetta', 'frontend', 2),
(29, 'LABEL_CONTACT_INFO', 'Info di contatto', 'Info di contatto', 'frontend', 1),
(30, 'LABEL_CONTACT_INFO', 'Contact infos', 'Info di contatto', 'frontend', 2),
(31, 'LABEL_CONTACT_INFO_TEXT', 'Ecco tutti i miei recapiti, se desideri informazioni o chiarimenti non esitare a contattarmi!', 'Info di contatto testo', 'frontend', 1),
(32, 'LABEL_CONTACT_INFO_TEXT', 'Here are all my contacts, if you want information or clarification do not hesitate to contact me!', 'Info di contatto testo', 'frontend', 2),
(33, 'LABEL_PHONE', 'Telefono', 'Telefono', 'frontend', 1),
(34, 'LABEL_PHONE', 'Phone', 'Telefono', 'frontend', 2),
(35, 'LABEL_SURNAME', 'Cognome', 'Cognome', 'frontend', 1),
(36, 'LABEL_SURNAME', 'Surname', 'Cognome', 'frontend', 2),
(37, 'LABEL_MESSAGE', 'Messaggio', 'Messaggio', 'frontend', 1),
(38, 'LABEL_MESSAGE', 'Message', 'Messaggio', 'frontend', 2),
(39, 'LABEL_SEND_MESSAGE', 'INVIA MESSAGGIO', 'INVIA MESSAGGIO', 'frontend', 1),
(40, 'LABEL_SEND_MESSAGE', 'SEND MESSAGE', 'INVIA MESSAGGIO', 'frontend', 2),
(41, 'LABEL_CONTACT_ME', 'Scrivimi', 'Scrivimi / contattami', 'frontend', 1),
(42, 'LABEL_CONTACT_ME', 'Contact me', 'Scrivimi / contattami', 'frontend', 2),
(43, 'LABEL_SEE_MORE', 'Vedi di più', 'Vedi di più', 'frontend', 1),
(44, 'LABEL_SEE_MORE', 'See more', 'Vedi di più', 'frontend', 2),
(45, 'MSG_SEND_CONTACT_US', 'Ti ringrazio per avermi contattato.<br>Risponderò appena possibile!', 'Messaggio contatto', 'frontend', 1),
(46, 'MSG_SEND_CONTACT_US', 'Thank you for contacting me.<br/>I will answer as soon as possible!', 'Messaggio contatto', 'frontend', 2),
(47, 'LABEL_INDEX_CLAIM_MOBILE', 'Musicista Lezioni Matrimoni', 'Etichette in home page sotto il nome per mobile', 'frontend', 1),
(48, 'LABEL_INDEX_CLAIM_MOBILE', 'Musician Lessons Weddings', 'Etichette in home page sotto il nome per mobile', 'frontend', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL,
  `label_page_url` varchar(250) NOT NULL,
  `ordine_menu` int(5) NOT NULL,
  `nome_menu` varchar(255) NOT NULL,
  `testo_menu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`, `label_page_url`, `ordine_menu`, `nome_menu`, `testo_menu`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', '', '', 0, '', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(5, 'Su di me', 'it/about', 1, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'Su di me'),
(7, 'Contatti', 'it/contatti', 1, 'frontend/Home/contatti', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contatti'),
(18, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(19, 'Home', 'en/home', 2, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(22, 'About', 'en/about', 2, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'About me'),
(24, 'Contacts', 'en/contacts', 2, 'frontend/Home/contatti', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contacts'),
(35, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(36, 'Gallery', 'it/gallery', 1, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(37, 'Gallery', 'en/gallery', 2, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(40, 'Matrimoni', 'it/matrimoni', 1, 'frontend/Home/matrimoni', 'statica', '', 'PAGE_WEDDINGS_URL', 2, 'MENU_WEDDINGS', 'Matrimoni'),
(41, 'Weddings', 'en/weddings', 2, 'frontend/Home/matrimoni', 'statica', '', 'PAGE_WEDDINGS_URL', 2, 'MENU_WEDDINGS', 'Weddings'),
(42, 'Lessons', 'en/lessons', 2, 'frontend/Home/lezioni', 'statica', '', 'PAGE_LESSONS_URL', 2, 'MENU_LESSONS', 'Lessons'),
(43, 'Lezioni', 'it/lezioni', 1, 'frontend/Home/lezioni', 'statica', '', 'PAGE_LESSONS_URL', 2, 'MENU_LESSONS', 'Lezioni'),
(44, 'Reviews', 'en/reviews', 2, 'frontend/Home/recensioni', 'statica', '', 'PAGE_REVIEWS_URL', 5, 'MENU_REVIEWS', 'Reviews'),
(45, 'Recensioni', 'it/recensioni', 1, 'frontend/Home/recensioni', 'statica', '', 'PAGE_REVIEWS_URL', 5, 'MENU_REVIEWS', 'Recensioni');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `description` longtext NOT NULL,
  `description2` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `image2` varchar(250) NOT NULL,
  `video` varchar(250) NOT NULL,
  `background_img` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `code`, `title`, `meta_description`, `description`, `description2`, `image`, `image2`, `video`, `background_img`, `id_lingua`) VALUES
(1, 'HOME', 'Home', 'sdfdsf', '', '', '', '', '', '', 1),
(3, 'ABOUT', 'Su di me', 'Questa sono io, se hai bisogno di maggiori informazioni,<br> di un consiglio o  semplicemente se preferisci parlare di persona con me<br> ti invito\r\na contattarmi e ti risponderò prima possibile.\r\n', 'Una bimba che a 10 anni inizia a studiare musica, pianoforte, dopo avere fatto danza classica ( altra mia passione di sempre ) e cantato come voce scelta in tanti cori della scuola… che ormai ragazza si diploma in questo strumento in uno dei Conservatori con la scuola  pianistica più dura e severa: S.Pietro a Majella, a Napoli….\r\nChe intraprende l’insegnamento e continua a suonare qua e là ma…\r\nla voce dell\'anima riprende a gridare dentro: Canta….CANTA!\r\nOk, ricevuto…riprendo a cantare in cori polifonici, girando l’Europa, in competion di vario genere, costruendo una conoscenza sulla musica antica meravigliosa!  ma…..la vocina c’è ancora…NO!\r\nInizio allora a studiare canto lirico mi avvicino all’opera, all’operetta e….alla canzone napoletana.\r\n', 'Eccomi tornata!!! Nella mia amata Napoli, seconda città. Dove il mio cuore batteva e batte tuttora. Quando da piccola stavo incollata davanti alla televisione a guardare le commedie di Eduardo e ad ascoltare quella musica….il parlare napoletano! Quanto mi ha affascinata!!!\r\nE allora, dopo avere studiato tanta tanta musica ( aggiungete pure jazz, pop, rock, e tutto quello che oggi c’è) avere fatto serate musicali di ogni genere ( compreso il liscio…..) e concerti dove sono approdata???? \r\nAL TEATRO NAPOLETANO, AL VARIETA’ AL CABARET, ALL’ AVANSPETTACOLO !!!\r\nOggi Emanuela,  oltre ad essere da sempre insegnante di pianoforte, si dedica, con tanta soddisfazione ( perché al pubblico che ama il genere, PIACE!...al teatro, ai concerti, agli spettacoli.\r\nAdesso mi dovete conoscere…sono proprio unica…..\r\n', '2667f-web1.jpg', '0c230-web2.jpg', '', '3d20a-about_em.jpg', 1),
(6, 'PRIVACY', 'Privacy', 'Scopri in che modo vengo trattati i tuoi dati sensibili,<br>l\'utilizzo dei cookie e dei dati di sessione in questo sito.', '<b>Cookie policy</b><br>\r\nIn questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie è una piccola particella di informazioni che viene salvata sul dispositivo dell\'utente che visita un sito web. Il cookie non contiene dati personali e non può essere utilizzato per identificare l\'utente all\'interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l\'efficienza del nostro portale.\r\n<br><br>\r\nPer avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornirà indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br>\r\nIn questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L\'anonimato dell\'utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.\r\n<br><br>\r\nRibadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l’utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell’utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l’accesso in modalità sicura oppure le funzioni di controllo e prevenzione delle frodi.<br><br>\r\nNon è obbligatorio acquisire il consenso alla operatività dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro <br>operatività comporterà l’impossibilità di una corretta navigazione sul Sito e/o la impossibilità di fruire dei servizi, delle pagine, delle funzionalità o dei contenuti ivi disponibili.\r\n<br><br>\r\nTutti i dati inseriti dai nostri clienti all\'interno di moduli e procedure di contatto verranno utilizzati esclusivamente per il completamento degli stessi. \r\n<br><br>\r\nIn ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalità od utilizzo.', '', '', '', '', '37646-matrimonio1.jpg', 1),
(7, 'CONTACTS', 'Contatti', 'Se hai bisogno di maggiori informazioni, di un consiglio o <br>semplicemente se preferisci parlare di persona con me ti invito<br>a contattarmi e ti risponderò prima possibile.', 'Se hai bisogno di maggiori informazioni, di un consiglio o <br>semplicemente se preferisci parlare di persona con me ti invito<br>a contattarmi e ti risponderò prima possibile.', '', '', '', '', '', 1),
(8, 'GALLERY', 'Gallery', 'Ecco una galleria delle nostre immagini più belle.', 'Vi mostro la galleria delle immagini più belle raccolte in questi ultimi anni.', '', '', '', '', '', 1),
(9, 'HOME', 'Home', 'Welcome to Ma Chlò\' s website, selling quality garments based on artwork with worldwide shipping.', '', '', '', '', '', '', 2),
(11, 'ABOUT', 'About me', 'This is me, if you need more information,<br/>advice or simply if you prefer to talk in person with me, I invite you\r\nto contact me<br/>and I will answer as soon as possible.', 'A girl who at 10 years starts studying music, piano, after doing classical dance (another passion of mine) and sung as a voice chosen in many choirs of the school ... now a girl graduates in this instrument in one of the Conservatories with the harder and more rigorous piano school: S. Pietro a Majella, Naples ....\r\nWho undertakes the teaching and keeps playing here and there but ...\r\nthe voice of the soul starts shouting inside: Sing ... SING!\r\nOk, received ... I resume singing in polyphonic choirs, touring Europe, in various competitions, building a knowledge of wonderful ancient music! but ... .. the voice is still there ... NO!\r\nSo I start studying lyric singing and I approach the opera, operetta and ... the Neapolitan song.', 'Here I am back !!! In my beloved Naples, second city. Where my heart was beating and still beating. When I was a child I was glued to the television watching the Eduardo comedies and listening to that music ... .the Neapolitan talk! How fascinated me!And then, after studying so much music (add jazz, pop, rock, and everything that there is today) have made musical evenings of all kinds (including the smooth ... ..) and concerts where I landed ?? ?AT THE NAPOLETANO THEATER, AL VARIETY AT THE CABARET, AT THE AVANSPECTACOLO !!!Today Emanuela, besides being always a piano teacher, dedicates herself with such satisfaction (because to the public who loves the genre, PIACE! ... to theater, concerts, shows.Now you must know me ... I\'m really unique ...', '337ec-web1.jpg', 'ae3ee-web2.jpg', '', '14fbc-about_em.jpg', 2),
(14, 'PRIVACY', 'Privacy', 'Find out how we treat your sensitive data,<br>use cookies, and sessions data in this site.', '<b>Cookie policy</b><br>\r\nThis site uses some technical cookies that are used for navigation. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the device of the user who visits a website. The cookie does not contain personal data and can not be used to identify you within other websites, including the analytics provider\'s website. Cookies can also be used to store the preferred settings, such as language and country, in order to make them immediately available on the next visit. We do not use IP addresses or cookies to personally identify users. We use the web analysis system in order to increase the efficiency of our portal.\r\n<br><br>\r\nFor more information on cookies we suggest you visit www.allaboutcookies.org which will provide you with information on how to manage according to your preferences, and possibly delete cookies depending on the browser you are using.\r\nOn this website, we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browser and users\' computers. This information is only examined for statistical purposes. The anonymity of the user is respected. Information on the operation of open source web analytics software Google Analytics.\r\n<br><br>\r\nWe reiterate that on the site are operating only technical cookies (such as those listed above) necessary to navigate and essential such as authentication, validation, management of a browsing session and prevention of fraud and allow for example: to identify if the user has had regularly access to areas of the site that require prior authentication or user validation and management of sessions relating to various services and applications or data retention for secure access or fraud control and prevention functions. <br>\r\nIt is not mandatory to acquire the consent to the operation of only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial to their operation will make it impossible to correctly browse the site and / or the inability to use the services, pages, features or content available there.\r\n<br><br>\r\nAll data entered by our customers within contact forms and procedures will be used exclusively for the completion of the same.\r\n<br><br>\r\nIn any case, we confirm that the data used and stored for the purpose of the service will never be transferred to third parties under any circumstances for any purpose or use.', '', '', '', '', '', 2),
(15, 'CONTACTS', 'Contacts', 'If you need more information, advice<br/> or simply if you prefer to talk in person with me,<br/> I invite you to contact me and I will answer as soon as possible.', 'If you need more information, advice<br/> or simply if you prefer to talk in person with me,<br/> I invite you to contact me and I will answer as soon as possible.', '', '', '', '', '', 2),
(16, 'GALLERY', 'Gallery', 'This is our best images gallery.', 'This is the picture gallery from my portfolio.', '', '', '', '', '', 2),
(17, 'WEDDINGS', 'Matrimoni', 'La musica per il tuo Matrimonio.', 'La cerimonia di matrimonio è il momento più importante ed emozionante. E allora perché troppe volte trascurarla, o scegliere un accompagnamento che svilisca il tutto? Una voce toccante ed angelica, un accompagnamento musicale che valorizzi ed intensifichi l’emozione nei vari momenti del rito, un prezzo assolutamente concorrenziale, nonostante la mia estrema competenza e preparazione, date da titoli specifici ed una quindicinale esperienza nel settore cerimonie?', 'Scegliete con cura chi dovrà esprimere le vostra emozione nel momento più importante del vostro giorno più bello!<br>Per le coppie che sceglieranno la mia musica offro in omaggio il CD contenente tutti i brani della loro cerimonia!<br>Sarà un dolcissimo ricordo che vi accompagnerà sempre, oltre che un prezioso ausilio all’operatore che realizzerà il vostro video di nozze.', 'matrimonio1.jpg', 'matrimonio2.jpg', 'J2802kgDbc8', '', 1),
(18, 'WEDDINGS', 'Weddings', 'Music for your wedding', 'The wedding ceremony is the most important and exciting moment. So why too often neglect it, or choose an accompaniment that debases the whole? A touching and angelic voice, a musical accompaniment that enhances and intensifies the emotion in the various moments of the rite, an absolutely competitive price, despite my extreme competence and preparation, given by specific titles and a fortnightly experience in the ceremonies sector?', 'Choose carefully who will express your emotion in the most important moment of your most beautiful day!\r\nFor couples who choose my music I offer a free CD containing all the songs of their ceremony!\r\nIt will be a sweet memory that will always accompany you, as well as a precious aid to the operator who will make your wedding video.', '37646-matrimonio1.jpg', 'eb764-matrimonio2.jpg', 'J2802kgDbc8', '', 2),
(19, 'LESSONS', 'Lezioni', 'Lezioni di musica e canto', '<b>Lezioni individuali di pianoforte (classico, leggero, pianobar) e/o tastiera, anche a domicilio</b><br>\r\nLa musica è energia totale. Chi riesce a viaggiare sulla sua lunghezza d\'onda lo sa. È un discorso di pancia,  di vibrazione,  di emozione. Il fatto che sia stata codificata in simboli dal significato uguale per il mondo intero,  non deve far dimenticare questo.<br/> \r\nE quando un bambino dice che gli piace la musica,  che vuole impararla,  vuol dire semplicemente che vuole entrarci tutto,  con tutto se stesso con più consapevolezza. \r\nPer questo,  oltre al pianoforte,  mi piace farlo esprimere anche attraverso la voce, lasciarlo libero di farla uscire...<br/> \r\nPerché questo è il modo più naturale che la vita ci ha dato per entrare nel mondo dei suoni. \r\nE sarà il bambino,  il ragazzo,  che mi farà capire via via dove dobbiamo andare.<br/>\r\nSi,  perché la musica è un mondo senza confini. ', '', 'lezioni1.jpg', 'lezioni2.jpg', 'NmQrW_hLDJw', '', 1),
(20, 'LESSONS', 'Lessons', 'Music and sing lessons', '<b>Individual piano lessons (classic, light, pianobar) and / or keyboard, even at home </b> <br/>\r\nMusic is total energy. Who can travel on its wavelength knows it. It is a speech of the stomach, of vibration, of emotion. The fact that it has been codified in symbols of the same meaning for the whole world, must not make you forget this.<br/>\r\nAnd when a child says that he likes music, that he wants to learn it, it simply means that he wants to enter everything, with all of himself with more awareness.\r\nFor this, in addition to the piano, I like to express it also through the voice, leave it free to let it out ...<br/>\r\nBecause this is the most natural way that life has given us to enter the world of sounds.\r\nAnd it will be the child, the boy, who will make me understand where we have to go.<br/>\r\nYes, because music is a world without borders.', '', 'lezioni1.jpg', 'lezioni2.jpg', 'NmQrW_hLDJw', '', 2),
(21, 'REVIEWS', 'Recensioni', 'Recensioni su Emanuela Mari', 'Le persone che mi conoscono e con cui ho lavorato hanno detto qualcosa su di me.', '', '', '', '', '', 1),
(22, 'REVIEWS', 'Reviews', 'Reviews about Emanuela Mari', 'Read all the reviews from people who knows me and work with me.', '', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `reviews`
--

CREATE TABLE `reviews` (
  `review_id` int(11) NOT NULL,
  `review_data` date NOT NULL,
  `review_messaggio` text NOT NULL,
  `review_nomi` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `reviews`
--

INSERT INTO `reviews` (`review_id`, `review_data`, `review_messaggio`, `review_nomi`) VALUES
(1, '2018-08-21', 'Grazie mille per la musica straordinaria al nostro matrimonio !!!', 'Michela e Pasquale'),
(2, '2018-08-13', 'Sono troppo felice di aver scelto Sposashop, il signore è stato sempre gentile e disponibile con me e le partecipazioni mi sono arrivate in due giorni nonostante abiti in Francia! E sono molto più belle che nella foto, stupende! Erano tutte ben protette... Tutto bellissimo!', 'Piero e Carla');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE `stato_descrizione` (
  `id_stato_descrizione` int(11) NOT NULL,
  `testo_stato_descrizione` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`id_stato_descrizione`, `testo_stato_descrizione`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$VIIUZuhwWAcG.TrOVOQU2ucWA/yaLIuFclm0Tl1B9.x32jXOTimzK', '', 'roberto.rossi77@gmail.com', '', 'rgH4-1JUJTBA.3ZhyB48Re2936ab16c301663644', 1516286626, NULL, 1268889823, 1535033796, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(2, '127.0.0.1', 'info@emanuelamari.it', '$2y$08$uM5YThwAvA0ALmWIDJqoY.Ap6aNgAcQb5VRq2qKRfpXdzS9bWn226', NULL, 'info@emanuelamari.it', NULL, NULL, NULL, NULL, 0, 1535034065, 1, 'Emanuela', 'Mari', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(3, 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `videos`
--

CREATE TABLE `videos` (
  `v_id` int(11) NOT NULL,
  `v_link` varchar(250) NOT NULL,
  `v_desc` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `videos`
--

INSERT INTO `videos` (`v_id`, `v_link`, `v_desc`) VALUES
(1, 'https://www.youtube.com/embed/J2802kgDbc8', 'Soprano per matrimonio'),
(2, 'https://www.youtube.com/embed/IsH9HXsdv_A', 'Fratello sole sorella luna'),
(3, 'https://www.youtube.com/embed/SwZQgNjmhCg', 'Cerimonia');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indici per le tabelle `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indici per le tabelle `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indici per le tabelle `demo_audio`
--
ALTER TABLE `demo_audio`
  ADD PRIMARY KEY (`da_id`);

--
-- Indici per le tabelle `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indici per le tabelle `eventi`
--
ALTER TABLE `eventi`
  ADD PRIMARY KEY (`evento_id`);

--
-- Indici per le tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  ADD PRIMARY KEY (`ig_id`);

--
-- Indici per le tabelle `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indici per le tabelle `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  ADD PRIMARY KEY (`id_lingue_labels_lang`);

--
-- Indici per le tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indici per le tabelle `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indici per le tabelle `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`review_id`);

--
-- Indici per le tabelle `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`id_stato_descrizione`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indici per le tabelle `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`v_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT per la tabella `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `demo_audio`
--
ALTER TABLE `demo_audio`
  MODIFY `da_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `eventi`
--
ALTER TABLE `eventi`
  MODIFY `evento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  MODIFY `ig_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  MODIFY `id_lingue_labels_lang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT per la tabella `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT per la tabella `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT per la tabella `reviews`
--
ALTER TABLE `reviews`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  MODIFY `id_stato_descrizione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `videos`
--
ALTER TABLE `videos`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
